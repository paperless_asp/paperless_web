<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
	<%@ include file="/WEB-INF/views/include_web/header_src.jsp"%>
	<script type="text/javascript">
	$(document).ready(function(){
	
	});
	</script>
</head>
<body class="bg">
<div id="wrap">
	<!-- header -->
	<%@ include file="/WEB-INF/views/include_web/header_top.jsp"%>
	<ul class="sub_menu">
		<li class="first"><a href="#" class="menu01"><span>신용·한도 조회</span></a></li>
		<li><a href="/${map.loanId }/confirm/info" class="menu02"><span>대출신청내역</span></a></li>
	</ul>
	<!-- //header -->
	<!-- gnb -->
	<%@ include file="/WEB-INF/views/include_web/gnb.jsp"%>
	<!-- //gnb -->
	
	<!-- contents -->
	<div id="container">
		<div class="main_img"><img src="/static/img/main_img.png"></div>
		<div class="main_txt">
			<p><strong>보다 쉽고 간편하게 처리하세요.</strong></p>
			<p>빠르게 대출을 신청 하기에는 <br>
			찾아가기엔 먼 은행, 넘쳐나는 서류</p>
		</div>

	</div>
	<!-- //contents --> 

	<!-- 빠른 상담신청 -->
	<%@ include file="/WEB-INF/views/include_web/fast_app.jsp"%>
	<!-- //빠른 상담신청 -->
</div>
</body>
</html>