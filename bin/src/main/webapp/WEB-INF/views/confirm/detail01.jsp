<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
	<%@ include file="/WEB-INF/views/include_web/header_src.jsp"%>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#authRequest").bind("click",function(){
			if($("#bankList option:selected").val()){
				alert("입금할 은행을 선택하세요.");
				return;
			}
			
		});
	});
	</script>
</head>
<body>
<c:set var="psEntity" value="${map.psEntity }"/>
<div id="wrap">
	<!-- header -->
	<%@ include file="/WEB-INF/views/include_web/header_top.jsp"%>
	<!-- //header -->
	<!-- gnb -->
	<%@ include file="/WEB-INF/views/include_web/gnb.jsp"%>
	<!-- //gnb -->
	
	<!-- contents -->
	<div id="container">

		<div class="contbox bg">
			<h2 class="stit">대출신청 정보</h2>
			<!-- wrt_table -->
			<div class="wrt_table">
			    <table cellspacing="0" border="1">
			        <caption>대출신청 정보</caption>
			        <colgroup>
			            <col style="width:40%;">
			            <col style="width:auto;">
			        </colgroup>
			        
			        <tbody>
				        <tr class="first">
				            <th scope="row">신청일</th>
				            <td>${psEntity.reg_date } </td>
				        </tr>
				        <tr>
				            <th scope="row">대출금액</th>
				            <td><fmt:formatNumber value="${psEntity.require_amount }" pattern="#,###" /> 만원</td>
				        </tr>
				        <tr>
				            <th scope="row">진행상태</th>
				            <td>서류 작성 중</td>
				        </tr>
			        </tbody>
			    </table>
			</div>
			<!-- //wrt_table -->
		</div>

		<div class="contwbox bg tline">
			<h2 class="stit">진행상태</h2>
			<!-- lst_table -->
			<div class="lst_table">
			    <table cellspacing="0" border="1">
			        <caption>진행상태</caption>
			        <colgroup>
			            <col style="width:70%;">
			            <col style="width:auto;">
			        </colgroup>
			        <thead>
				        <tr>
				            <th scope="col" class="al">단계</th>
				            <th scope="col">상태</th>
				        </tr>
			        </thead>
			        <tbody>
				        <tr>
				            <td class="al">전화상담 신청</td>
				            <td><span class="progress pg_finish">${psEntity.call_yn }완료</span></td>
				        </tr>
				        <tr>
				            <td class="al">신청서 전자서명</td>
				            <td><span class="progress pg_not">${psEntity.eform_yn }미진행</span></td>
				        </tr>
				        <tr>
				            <td class="al drop">계좌 인증</td>
				            <td><span class="progress pg_ing">${psEntity.account_verify_yn }진행중</span></td>
				        </tr>
				        <!-- 계좌 인증 -->
				        <tr class="drop_cont">
				        	<td colspan="2">
				        		<!-- 등록 전 -->
				        		<dl class="confirm_form01 account_cont">
									<dt>인증 계좌 등록</dt>
									<dd class="form">
										<input type="text" name="" id="">
										<button type="button" class="btn bc_purple">인증확인</button>
									</dd>
								</dl>
				        		<ul class="lnc_txt">
									<li>※ 먼저, 등록 계좌에서 입금 내역을 확인하세요.</li>
									<li>※ 입금자 명의 4자리 코드를 입력해 인증을 완료하세요.</li>
								</ul>
								<!-- //등록 전 -->
								<!-- 등록완료 -->
								<!-- <dl class="confirm_form01 account_cont">
									<dt>정상적으로 등록 계좌를 통해 인증이 처리되었습니다.</dt>
								</dl> -->
								<!-- 등록완료 -->
				        	</td>
				        </tr>
				        <!-- //계좌 인증 -->
				        <tr>
				            <td class="al drop">추가 서류 제출 (전용촬영 APP)</td>
				            <td><span class="progress pg_not">재요청</span></td>
				        </tr>
				        <!-- 추가서류제출 -->
					<c:choose>
				        <c:when test="${!empty map.efEntityList}">
				        <tr class="drop_cont">
				        	<td colspan="2">
				        		<ul class="document_photo">
				        		<c:forEach items="${map.progressList}" var="list">
				        			<li>
				        				<p>신분증(앞/뒤)</p>
				        				<div><span class="progress pg_finish">완료</span></div>
				        			</li>
				        		</c:forEach>
				        			<li>
				        				<p>등기부등본</p>
				        				<div><span class="progress pg_not">재요청</span></div>
				        			</li>
				        			<li>
				        				<p>집문서</p>
				        				<div><span class="progress pg_not">미진행</span></div>
				        			</li>
				        		</ul>
				        		<ul class="lnc_txt">
									<li>※ 해당 서류 내용을 정확히 확인할 수 있도록 촬영하세요.</li>
								</ul>
				        	</td>
				        </tr>
				        </c:when>
					</c:choose>
				        <!-- //추가서류제출 -->
			        </tbody>
			    </table>
			</div>
			<!-- //lst_table -->

			<ul class="lnc_txt lrm">
				<li>※ 모든 단계를 완료하면, 신청한 대출금액이 입금됩니다.</li>
			</ul>
		</div>

		<div class="btm_btnbox">
			<a href="#" class="btn bc_wgray">신청완료</a>
			<a href="#" class="btn bc_gray">대출신청확인으로 이동</a>
		</div>

		

	</div>
	<!-- //contents -->
</div>

<!-- 입금계좌인증 레이어팝업 -->
<div class="layerpop" style="display:none;">
	<div class="layer_header">
		<strong class="tit">입금계좌인증</strong>
		<button tyle="button" class="layer_close">닫기</button>
	</div>
	<div class="layer_content">
		<div class="contbox bg">
			<ul class="layer_accountadd">
				<li>
					<div class="tit">이름</div>
					<div class="cont">홍길동</div>
				</li>
				<li>
					<div class="tit">은행</div>
					<div class="cont">
						<select name="" id="bankList" class="select_box">
						    <option value="">입금할 은행을 선택하세요.</option>
						    <option value="KB">국민은행</option>
						    <option value="IBK">기업은행</option>
						</select>
					</div>
				</li>
				<li>
					<div class="tit">계좌번호</div>
					<div class="cont"><input type="text" name="" id="accountNumber" style="width:100%;" placeholder="계좌번호를 입력하세요."></div> 
				</li>
			</ul>
		</div>

		<div class="btm_btnbox02">
			<a href="#" id="authRequest" class="btn bc_purple">계좌등록 및 인증요청</a>
		</div>

		<div class="contbox">
			<p class="account_steptit">입금 계좌 인증 절차</p>
			<ol class="account_steplst">
				<li>신청 완료 후, 대출금이 입금될 은행과 계좌번호를 등록합니다.</li>
				<li>등록한 계좌로 1원 자동 입금이 진행되며, 해당 은행의 뱅킹 서비스를 통해 입금 내역을 확인합니다.</li>
				<li>입금 내역 중 입금자 명에 표기된 코드(영문, 숫자 4자리)를 확인합니다.</li>
				<li>본 서비스에 다시 접속해, 4자리 코드를 계좌 인증 메뉴에 입력 후 계좌 인증 절차를 완료합니다.</li>
			</ol>
		</div>
	</div>
</div>
<!-- //입금계좌인증 레이어팝업 -->

</body>
</html>