<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
	<%@ include file="/WEB-INF/views/include_web/header_src.jsp"%>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#auth_res").bind("click",function(){
			if($("#tel_number").val() == ""){
				alert("휴대폰 번호를 확인해주십시오.");
				return;
			}
			alert("인증번호가 전송되었습니다.");
			$("#auth_confirm").removeClass("bc_gray");
			$("#auth_confirm").addClass("bc_purple");
		});
		
		$("#auth_confirm").bind("click",function(){
			if($("#tel_number").val() == ""){
				alert("휴대폰 번호를 확인해주십시오.");
				return;
			}
			if($("#auth_number").val() == ""){
				alert("인증 번호를 확인해주십시오.");
				return;
			}
			window.location.href="detail01";
		});
	});
	</script>
</head>
<body class="bg">
<div id="wrap">
	<!-- header -->
	<%@ include file="/WEB-INF/views/include_web/header_top.jsp"%>
	<!-- //header -->
	<!-- gnb -->
	<%@ include file="/WEB-INF/views/include_web/gnb.jsp"%>
	<!-- //gnb -->
	
	<!-- contents -->
	<div id="container">
		<div class="top_infotxt">
			정보보호를 위해<br>
			<span>휴대폰 인증 진행 후 내역을 조회</span>할 수 있습니다.<br>
			인증번호 요청 후 <span>정확히 입력해 주세요.</span>
		</div>	

		<div class="contbox">
			<dl class="confirm_form01">
				<dt>휴대폰 번호</dt>
				<dd class="form">
					<input type="text" name="" id="tel_number">
					<button type="button" class="btn bc_purple" id="auth_res">인증번호 요청</button>
				</dd>
			</dl>
			<dl class="confirm_form01 tline">
				<dt>인증번호 입력</dt>
				<dd class="form">
					<input type="text" name="" id="auth_number">
					<button type="button" class="btn bc_gray" id="auth_confirm">인증확인</button>
				</dd>
			</dl>
			<ul class="lnc_txt">
				<li>※ 인증번호를 수신하지 못할 경우, 인증번호 재발송을 선택하세요.</li>
			</ul>
		</div>

	</div>
	<!-- //contents -->
</div>

<!-- loading -->
<div class="loading_box" style="display:none;">
	<div class="mask_area"></div>	
	<div class="loading_cont"><img src="/static/img/loading_1.gif"></div>
</div>
<!-- //loading -->

</body>
</html>