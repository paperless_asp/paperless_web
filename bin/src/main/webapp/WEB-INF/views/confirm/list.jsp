<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
	<%@ include file="/WEB-INF/views/include_web/header_src.jsp"%>
	<script type="text/javascript">
	$(document).ready(function(){
	
	});
	
	function fn_goDetail(seq){
		window.location.href="detail01?seq="+seq;
	}
	</script>
</head>
<body>
<div id="wrap">
	<!-- header -->
	<%@ include file="/WEB-INF/views/include_web/header_top.jsp"%>
	<!-- //header -->
	<!-- gnb -->
	<%@ include file="/WEB-INF/views/include_web/gnb.jsp"%>
	<!-- //gnb -->
	
	<!-- contents -->
	<div id="container">
		<div class="top_infotxt">
			모바일 대출신청을 선택해 <span>상세정보를 확인</span>하세요.
		</div>	

		<div class="contbox bg">
			<h2 class="stit">고객정보</h2>
			<!-- wrt_table -->
			<div class="wrt_table">
			    <table cellspacing="0" border="1">
			        <caption>신청자 기본정보</caption>
			        <colgroup>
			            <col style="width:40%;">
			            <col style="width:auto;">
			        </colgroup>
			        <tbody>
			        <tr class="first">
			            <th scope="row">이름</th>
			            <td>홍길동</td>
			        </tr>
			        <!-- <tr>
			            <th scope="row">주민등록번호</th>
			            <td>750111-*******</td>
			        </tr> -->
			        </tbody>
			    </table>
			</div>
			<!-- //wrt_table -->
		</div>

		<div class="contwbox bg tline">
			<h2 class="stit">모바일 대출 신청 내역</h2>
			<!-- lst_table -->
			<div class="lst_table">
			    <table cellspacing="0" border="1">
			        <caption>대출 신청 내역</caption>
			        <colgroup>
			            <col style="width:33%;">
			            <col style="width:33%;">
			            <col style="width:auto;">
			        </colgroup>
			        <thead>
				        <tr>
				            <th scope="col">신청일</th>
				            <th scope="col">대출금액</th>
				            <th scope="col">진행상태</th>
				        </tr>
			        </thead>
			        <tbody>
			        <c:choose>
				        <c:when test="${!empty map.progressList}">
							<c:forEach items="${map.progressList}" var="list">
							<tr onclick="fn_goDetail('${list.seq}')">
							    <td>${fn:substring(fn:replace(list.reg_date,"-","."),0, 10)}</td>
								<td><fmt:formatNumber value="${list.require_amount}" pattern="#,###" /> 만원</td>
							    <td>대출심사중</td>
							</tr>
							</c:forEach>
				         </c:when>
				         <c:otherwise>
					        <tr>
					            <td colspan="3">신청 내역이 존재하지 않습니다.</td>
					        </tr>
				         </c:otherwise>
					</c:choose>
			        </tbody>
			    </table>
			</div>
			<!-- //lst_table -->

			<ul class="lnc_txt lrm">
				<li>※ 서류 작성 중인 경우, 빠른 시간 내 관련 서류 제출 완료가 필요합니다.</li>
				<li>※ 신청 완료 후 서류 검토 과정에서 보완을 요청한 경우, 관련 서류를 확인한 후 양식에 맞게 다시 제출해야 합니다.</li>
			</ul>
		</div>

		

	</div>
	<!-- //contents -->
</div>
</body>
</html>