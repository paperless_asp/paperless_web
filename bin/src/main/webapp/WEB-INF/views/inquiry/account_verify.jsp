<!doctype html>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html lang="ko">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
	<link rel="stylesheet" type="text/css" href="/static/include/css/common.css">
	<!--[if IE]>
	<script type=”text/javascript” src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script type="text/javascript" src="/static/include/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="/static/include/js/common.js"></script>
</head>
<body>
<div id="wrap">
	<!-- header -->
	<div id="header">
		<h1><a href="#">모바일대출 상세정보</a></h1>
		<a href="#" class="back">back</a>
		<div class="gnb_menu">
			<p>side menu</p>
			<span class="lt"></span>
			<span class="rt"></span>
			<span class="lb"></span>
			<span class="rb"></span>
		</div>
	</div>
	<!-- //header -->
	<!-- gnb -->
	<div class="gnb_mask"></div>
	<nav id="gnb">
		<ul>
			<li><a href="#" class="menu01">신용/한도 조회</a></li>
			<li><a href="#" class="menu02">대출신청내역 조회</a></li>
			<li class="line"><span></span></li>
			<li><a href="#">고객문의</a></li>
			<li><a href="#">공지사항</a></li>
		</ul>
	</nav>
	<!-- //gnb -->
	
	<!-- contents -->
	<div id="container">

		<div class="contbox bg">
			<h2 class="stit">대출신청 정보</h2>
			<!-- wrt_table -->
			<div class="wrt_table">
			    <table cellspacing="0" border="1">
			        <caption>대출신청 정보</caption>
			        <colgroup>
			            <col style="width:40%;">
			            <col style="width:auto;">
			        </colgroup>
			        <tbody>
			        <tr class="first">
			            <th scope="row">신청일</th>
			            <td>2016.11.15</td>
			        </tr>
			        <tr>
			            <th scope="row">대출금액</th>
			            <td>500만원</td>
			        </tr>
			        <tr>
			            <th scope="row">진행상태</th>
			            <td>서류 작성 중</td>
			        </tr>
			        </tbody>
			    </table>
			</div>
			<!-- //wrt_table -->
		</div>

		<div class="contwbox bg tline">
			<h2 class="stit">진행상태</h2>
			<!-- lst_table -->
			<div class="lst_table">
			    <table cellspacing="0" border="1">
			        <caption>진행상태</caption>
			        <colgroup>
			            <col style="width:70%;">
			            <col style="width:auto;">
			        </colgroup>
			        <thead>
				        <tr>
				            <th scope="col" class="al">단계</th>
				            <th scope="col">상태</th>
				        </tr>
			        </thead>
			        <tbody>
				        <tr>
				            <td class="al">전화상담 신청</td>
				            <td><span class="progress pg_finish">완료</span></td>
				        </tr>
				        <tr>
				            <td class="al">신청서 전자서명</td>
				            <td><span class="progress pg_not">미진행</span></td>
				        </tr>
				        <tr>
				            <td class="al drop">계좌 인증</td>
				            <td><span class="progress pg_ing">진행중</span></td>
				        </tr>
				        <!-- 계좌 인증 -->
				        <tr class="drop_cont">
				        	<td colspan="2">
				        		<!-- 등록 전 -->
				        		<dl class="confirm_form01 account_cont">
									<dt>인증 계좌 등록</dt>
									<dd class="form">
										<input type="text" name="" id="">
										<button type="button" class="btn bc_purple">인증확인</button>
									</dd>
								</dl>
				        		<ul class="lnc_txt">
									<li>※ 먼저, 등록 계좌에서 입금 내역을 확인하세요.</li>
									<li>※ 입금자 명의 4자리 코드를 입력해 인증을 완료하세요.</li>
								</ul>
								<!-- //등록 전 -->
								<!-- 등록완료 -->
								<!-- <dl class="confirm_form01 account_cont">
									<dt>정상적으로 등록 계좌를 통해 인증이 처리되었습니다.</dt>
								</dl> -->
								<!-- 등록완료 -->
				        	</td>
				        </tr>
				        <!-- //계좌 인증 -->
				        <tr>
				            <td class="al drop">추가 서류 제출 (전용촬영 APP)</td>
				            <td><span class="progress pg_not">재요청</span></td>
				        </tr>
				        <!-- 추가서류제출 -->
				        <tr class="drop_cont">
				        	<td colspan="2">
				        		<ul class="document_photo">
				        			<li>
				        				<p>신분증(앞/뒤)</p>
				        				<div><span class="progress pg_finish">완료</span></div>
				        			</li>
				        			<li>
				        				<p>등기부등본</p>
				        				<div><span class="progress pg_not">재요청</span></div>
				        			</li>
				        			<li>
				        				<p>집문서</p>
				        				<div><span class="progress pg_not">미진행</span></div>
				        			</li>
				        		</ul>
				        		<ul class="lnc_txt">
									<li>※ 해당 서류 내용을 정확히 확인할 수 있도록 촬영하세요.</li>
								</ul>
				        	</td>
				        </tr>
				        <!-- //추가서류제출 -->
			        </tbody>
			    </table>
			</div>
			<!-- //lst_table -->

			<ul class="lnc_txt lrm">
				<li>※ 모든 단계를 완료하면, 신청한 대출금액이 입금됩니다.</li>
			</ul>
		</div>

		<div class="btm_btnbox">
			<a href="#" class="btn bc_wgray">신청완료</a>
			<a href="#" class="btn bc_gray">대출신청확인으로 이동</a>
		</div>

		

	</div>
	<!-- //contents -->
</div>

<!-- 입금계좌인증 레이어팝업 -->
<div class="layerpop" style="display:block;">
	<div class="layer_header">
		<strong class="tit">입금계좌인증</strong>
		<button tyle="button" class="layer_close">닫기</button>
	</div>
	<div class="layer_content">
		<div class="contbox bg">
			<ul class="layer_accountadd">
				<li>
					<div class="tit">이름</div>
					<div class="cont">홍길동</div>
				</li>
				<li>
					<div class="tit">은행</div>
					<div class="cont">
						<select name="" id="" class="select_box bank_select_box">
						    <option value="00">입금할 은행을 선택하세요.</option>
						   	<c:forEach var="item" items="${bankList}">
							     <option value="${item.bank_code_std}">${item.bank_name}</option>
							</c:forEach> X
						</select>
					</div>
				</li>
				<li>
					<div class="tit">계좌번호</div>
					<div class="cont"><input type="text" name="" id="" class="account_holder" style="width:100%;" placeholder="계좌번호를 입력하세요."></div> 
				</li>
				<li>
					<div class="tit">생년월일</div>
					<div class="cont"><input type="text" name="" id="" class="ssn_number" style="width:100%;" placeholder="생년월일 8자리를 입력하세요."></div> 
				</li>
			</ul>
		</div>

		<div class="btm_btnbox02">
			<div href="#" class="btn bc_purple request">계좌등록 및 인증요청</div>
		</div>

		<div class="contbox">
			<p class="account_steptit">입금 계좌 인증 절차</p>
			<ol class="account_steplst">
				<li>신청 완료 후, 대출금이 입금될 은행과 계좌번호를 등록합니다.</li>
				<li>등록한 계좌로 1원 자동 입금이 진행되며, 해당 은행의 뱅킹 서비스를 통해 입금 내역을 확인합니다.</li>
				<li>입금 내역 중 입금자 명에 표기된 코드(영문, 숫자 4자리)를 확인합니다.</li>
				<li>본 서비스에 다시 접속해, 4자리 코드를 계좌 인증 메뉴에 입력 후 계좌 인증 절차를 완료합니다.</li>
			</ol>
		</div>
	</div>
</div>
<!-- //입금계좌인증 레이어팝업 -->

</body>
</html>

<script type="text/javascript" src="static/include/js/jquery-1.12.4.min.js"></script>
<script type='text/javascript'>
$(document).ready(function(){
	/*
	$('.bank_select_box').change(function(){
		alert($(this).val());
	});
	*/
	$('.request').click(function(){
		
		var account_holder_number = $('.account_holder').val();
		var account_holder_code = $('.bank_select_box').val();
		var ssn_number = $('.ssn_number').val();
		
		if(account_holder_code == '00')
		{
			alert('입금하실 은행을 선택해 주세요.');	
			return;
		}
		
		if(account_holder_number.length == 0)
		{
			alert('계좌번호를 입력해 주세요.');
			return;
		}
		if(ssn_number.length != 8)
		{
			alert('생년월일 8자리를 입력해 주세요.')	;
			return;
		}
		
		$.ajax({
			url: "../inquiry/accountCheck",
			global: false,
			type: "POST",
			data: ({
				account_holder_number	: account_holder_number,
				account_holder_code		: account_holder_code,
				ssn_number				: ssn_number,
			}),
			dataType: "html",
			async: false,
			success: function (msg) {
				console.log("accountCheck response = " + msg);
				transderDeposit();
			}
		})
	});
	
	
	function transderDeposit(){
		var account_holder_number = $('.account_holder').val();
		var account_holder_code = $('.bank_select_box').val();
		var ssn_number = $('.ssn_number').val();
		
		$.ajax({
			url: "../inquiry/transderDeposit",
			global: false,
			type: "POST",
			data: ({
				account_holder_number	: account_holder_number,
				account_holder_code		: account_holder_code,
				ssn_number				: ssn_number,
				account_holder_name		: "홍길동",
			}),
			dataType: "html",
			async: false,
			success: function (msg) {
				console.log("transderDeposit response ="+msg);
			}
		})
	}
});
</script>