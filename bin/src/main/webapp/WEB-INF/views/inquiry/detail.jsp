<!doctype html>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<html lang="ko">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
	<link rel="stylesheet" type="text/css" href="/static/include/css/common.css">
	<!--[if IE]>
	<script type=”text/javascript” src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script type="text/javascript" src="/static/include/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="/static/include/js/common.js"></script>
</head>
<body class="bg">
<div id="wrap">
	<!-- header --> 
	<div id="header">
		<h1><a href="#">모바일대출 상세정보</a></h1>
		<a href="#" class="back">back</a>
		<div class="gnb_menu">
			<p>side menu</p>
			<span class="lt"></span>
			<span class="rt"></span>
			<span class="lb"></span>
			<span class="rb"></span>
		</div>
	</div>
	<!-- //header -->
	<!-- gnb -->
	<div class="gnb_mask"></div>
	<nav id="gnb">
		<ul>
			<li><a href="#" class="menu01">신용/한도 조회</a></li>
			<li><a href="#" class="menu02">대출신청내역 조회</a></li>
			<li class="line"><span></span></li>
			<li><a href="#">고객문의</a></li>
			<li><a href="#">공지사항</a></li>
		</ul>
	</nav>
	<!-- //gnb -->
	
	<!-- contents -->
	<div id="container">
		<div class="top_infotxt">
			신청하신 <span>모바일 대출을 담당자가 심사 중</span>이며,<br>
			<span>심사 완료 후 결과를 안내</span>해 드리겠습니다.
		</div>	

		<div class="contbox bg">
			<h2 class="stit">모바일 대출 상세정보</h2>
			<!-- wrt_table -->
			<div class="wrt_table">
			    <table cellspacing="0" border="1">
			        <caption>모바일 대출 상세정보</caption>
			        <colgroup>
			            <col style="width:40%;">
			            <col style="width:auto;">
			        </colgroup>
			        <tbody>
			        <tr class="first">
			            <th scope="row">이름</th>
			            <td>홍길동</td>
			        </tr>
			        <tr>
			            <th scope="row">주민등록번호</th>
			            <td>750111-*******</td>
			        </tr>
			        <tr>
			            <th scope="row">신청일</th>
			            <td>2016.11.15</td>
			        </tr>
			        <tr>
			            <th scope="row">대출금액</th>
			            <td>500만원</td>
			        </tr>
			        <tr>
			            <th scope="row">진행상태</th>
			            <td>대출 심사 중</td>
			        </tr>
			        </tbody>
			    </table>
			</div>
			<!-- //wrt_table -->
		</div>

		<div class="btm_btnbox">
			<a href="#" class="btn bc_gray">대출신청확인으로 이동</a>
		</div>

	</div>
	<!-- //contents -->
</div>
</body>
</html>