<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
	<%@ include file="/WEB-INF/views/include_web/header_src.jsp"%>
	<script type="text/javascript">
	$(document).ready(function(){
	
	});
	</script>
</head>
<body>
<div id="wrap">
	<!-- header -->
	<%@ include file="/WEB-INF/views/include_web/header_top.jsp"%>
	<!-- //header -->
	<!-- gnb -->
	<%@ include file="/WEB-INF/views/include_web/gnb.jsp"%>
	<!-- //gnb -->
	
	<!-- contents -->
	<div id="container">
		<ul class="step_box step03_p">
			<li class="step01"><span>본인인증</span></li>
			<li class="step02"><span>개인정보조회동의</span></li>
			<li class="step03 on"><span>조회완료</span></li>
		</ul>

		<div class="contbox bg">
			<h2 class="stit">신용정보조회결과</h2>
			<!-- wrt_table -->
			<div class="wrt_table">
			    <table cellspacing="0" border="1">
			        <caption>신용정보조회결과</caption>
			        <colgroup>
			            <col style="width:40%;">
			            <col style="width:auto;">
			        </colgroup>
			        <tbody>
			        <tr class="first">
			            <th scope="row">신용 등급</th>
			            <td>1등급</td>
			        </tr>
			        <tr>
			            <th scope="row">대출 이율</th>
			            <td>6.8%</td>
			        </tr>
			        <tr>
			            <th scope="row">대출 한도</th>
			            <td>최대 2,000만원</td>
			        </tr>
			        </tbody>
			    </table>
			</div>
			<!-- //wrt_table -->
		</div>

		<p class="txt_btmarea">본 신용조회는 개인신용평가에 영향을 미치지 않습니다.</p>
		<div class="btm_btnbox">
			<a href="#" class="btn bc_purple">모바일 대출 신청 요청</a>
		</div>

	</div>
	<!-- //contents -->
</div>


</body>
</html>