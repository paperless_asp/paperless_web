<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<div class="gnb_mask"></div>
<nav id="gnb">
	<ul>
		<li><a href="#" class="menu01">신용/한도 조회</a></li>
		<li><a href="#" class="menu02">대출신청내역 조회</a></li>
		<li class="line"><span></span></li>
		<li><a href="/${map.loanId}/info/inquire">고객문의</a></li>
		<li><a href="/${map.loanId}/info/notice">공지사항</a></li>
	</ul>
</nav>