<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@page isErrorPage="true" %>
<!DOCTYPE HTML>  
<html xmlns:th="http://www.thymeleaf.org">  
<head>
	<meta charset="utf-8" />
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
	<link rel="stylesheet" type="text/css" href="/static/include/css/common.css">
	<!--[if IE]>
	<script type=”text/javascript” src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script type="text/javascript" src="/static/include/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="/static/include/js/common.js"></script>
</head>
<body class="bg">
<div id="wrap">
	
	<!-- contents -->
	<div id="container">
		
		<div class="inquir_box">
			<h2>이용에 불편을 드려 죄송합니다.</h2>
			<p>일시적인 서버 점검상태일수 있으며, <br>
			장시간 유휴상태로 인하여<br>
			세션이 만료되었을수 있습니다.<br>
		</div>

	</div>
	<!-- //contents -->

</div>



</body>
</html>