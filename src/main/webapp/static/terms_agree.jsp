<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
	<%@ include file="/WEB-INF/views/include_web/header_src.jsp"%>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#agree_all").bind("click",function(){
			if($("#agree_all").is(":checked")){
				$("#agree01").prop("checked","checked");
				$("#agree02").prop("checked","checked");
				$("#agree03").prop("checked","checked");
			}else{
				$("#agree01").prop("checked","");
				$("#agree02").prop("checked","");
				$("#agree03").prop("checked","");
			}
		});
	});
	</script>
</head>
<body class="bg">
<div id="wrap">
	<!-- header -->
	<%@ include file="/WEB-INF/views/include_web/header_top.jsp"%>
	<!-- //header -->
	<!-- gnb -->
	<%@ include file="/WEB-INF/views/include_web/gnb.jsp"%>
	<!-- //gnb -->
	
	<!-- contents -->
	<div id="container">
		<div class="contwbox bg">
			<h2 class="stit">개인(신용)정보 및 거래약관 동의</h2>

			<ul class="hp_confirm terms_agreebox">
				<li>
					<p>
						<span class="form_btn">
			     			<input type="checkbox" name="agree01" id="agree01" >
							<label for="agree01">여신거래기본약관</label>
			     		</span>
					</p>
					<div><button type="button" class="btn bc_purple">보기</button></div>
				</li>
				<li>
					<p>
						<span class="form_btn">
			     			<input type="checkbox" name="agree02" id="agree02" >
							<label for="agree02">CMS자동이체약관</label>
			     		</span>
					</p>
					<div><button type="button" class="btn bc_purple">보기</button></div>
				</li>
				<li>
					<p>
						<span class="form_btn">
			     			<input type="checkbox" name="agree03" id="agree03" >
							<label for="agree03">인터넷개인신용대출 약정서</label>
			     		</span>
					</p>
					<div><button type="button" class="btn bc_purple">보기</button></div>
				</li>
			</ul>
			<p class="all_termscheck">
				<span class="form_btn">
			     	<input type="checkbox" name="agree_all" id="agree_all" >
					<label for="agree_all">전체 약관을 확인하였으며 동의합니다.</label>
			    </span>
			</p>
		</div>

		<div class="btm_btnbox">
			<a href="#" class="btn bc_gray">다음</a>
		</div>

	</div>
	<!-- //contents -->
</div>

<!-- 여신거래기본약관 레이어팝업 -->
<div class="layerpop" style="display:none;">
	<div class="layer_header">
		<strong class="tit">여신거래기본약관</strong>
		<button tyle="button" class="layer_close">닫기</button>
	</div>
	<div class="layer_content">
		<div class="contbox">
			<p class="layer_txt01">’06.9.25부터 시행(‘06.3.24 공포)된 주민등록법에 따라 주민등록번호의 부정사용에 따른 처벌이 강화 되었으므로 부정사용으로 처벌받지 않도록 주의하시기 바랍니다.</p>
			
			<ul class="layer_lst01">
				<li>3년 이하 징역 또는 1,000만원 이하 벌금</li>
				<li>재물ᆞ재산상 이익을 도모한 경우 뿐만 아니라, 단순 도용도 처벌</li>
			</ul>
		</div>
	</div>
</div>
<!-- //여신거래기본약관 레이어팝업 -->


</body>
</html>