<%@page import="java.net.InetAddress"%>
<%@page import="java.net.Inet4Address"%>
<%@page import="java.net.URL"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	boolean authorize = false;

	out.print("Start Get CanonicalHostName : " + new java.text.SimpleDateFormat("HH:mm:ss.SSS").format(new java.util.Date()) + "<br/>");

	InetAddress localhost = InetAddress.getLocalHost();
	InetAddress[] ips = InetAddress.getAllByName(localhost.getCanonicalHostName());
	
	out.print("End Get CanonicalHostName : " + new java.text.SimpleDateFormat("HH:mm:ss.SSS").format(new java.util.Date()) + "<br/>");
	
	StringBuffer sb = new StringBuffer();

	out.print("IP : " + localhost.getHostAddress() + "<br/>");
	out.print("HOSTNAME : " + localhost.getHostName() + "<br/>");
	
	for (int j = 0; j < ips.length; j++) {
		String IP = ips[j].getHostAddress();
		sb.append(IP);
		sb.append("<br/>");
	}
	
	out.print(sb.toString());
%>