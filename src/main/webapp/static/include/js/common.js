$(document).ready(function() {

	$('.gnb_menu').click(function(event){
		var $body = $('body');
		if($body.hasClass('menu_open')){
			$body.removeClass('menu_open');
		}else{
			$body.addClass('menu_open');
		}
	});
	$('.gnb_mask').click(function(){
		$('.gnb_menu').trigger('click');
	});

	//
	$('.drop').click(function(){
		var $this = $(this);
		$this.toggleClass('on');
		$this.parent().next('.drop_cont').toggle();
	});
	$('.drop + td>.progress').click(function(){ 
		$(this).parent().siblings('.drop').trigger('click');
	});

	//layer_close
	$('.layer_close').click(function(){
		var $this = $(this);
		$this.parent().parent().hide();
	});
	

	$('.fastapp_btn').click(function(){
		$(this).toggleClass('on');
		$('.fastapp_contents').toggle();
		$('.fast_mask').toggle();
	});
});