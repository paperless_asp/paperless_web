$(document).ready(function(){
	$("#fast_check").bind("click",function(){
		$("#call_start_time option").eq(0).attr("selected","selected");
		$("#call_end_time option").eq(23).attr("selected","selected");
	});
	
	$("#faseBtn").bind("click",function(){
		if(validation()){
			$.blockUI({ 
				/*css: {
		            border: 'none', 
		            padding: '15px', 
		            backgroundColor: '#000', 
		            '-webkit-border-radius': '10px', 
		            '-moz-border-radius': '10px', 
		            opacity: .5, 
		            color: '#fff' 
	        	}*/
				message :
					"<div class='loading_box' style='display:block;'><div class='mask_area'></div>"+
					"<div class='loading_cont'><img src='/static/img/loading_1.gif'/></div>"
			}); 
	        setTimeout($.unblockUI, 2000); 
			fast_app();
			$('.fast_mask').toggle();
			$('.fastapp_contents').toggle()
		}
	});
	
	$("#amount").keyup(function(){
		$(this).val(numberWithCommas($("#amount").val()));
	});
	
	$("#tel_number").keyup(function(){
		$(this).val(onlyNumber($("#tel_number").val()));
	});
});

function validation(){
	if(!onlyHan("user_name" , "한글만 가능합니다.")){
		$("#user_name").focus();
		return false;
	}
	if(!checkVal("tel_number")){
		$("#tel_number").focus();
		return false;
	}
	if(!checkVal("amount")){
		$("#amount").focus();
		return false;
	}
	if(!checkLength("tel_number")){
		$("#tel_number").focus();
		return false;
	}
	var amount = onlyNumber($("#amount").val());
	
	if((amount*1) < ($("#amount").attr("data-min")*1)){
		swal("","입력하신 희망 대출금액이 최소금액 보다 낮습니다.");
		$("#amount").focus();
		$("#amount").val("");
		return false;
	}
	
	if((amount*1) > ($("#amount").attr("data-max")*1)){
		swal("","입력하신 희망 대출금액이 최대금액이 보다 높습니다.");
		$("#amount").focus();
		$("#amount").val("");
		return false;
	}
	
	if(!$("#fast_check").prop("checked")){
		if($("#call_start_time option:selected").val() >= $("#call_end_time option:selected").val()){
			swal("","희망상담시간을 확인해주십시오.");
			return false;
		}else{
			return true;
		}
	}else{
		return true;
	}
}

function fast_app(){
	//대출신청 저장
	//${map.loanId }
	var method="POST";
	var requestUrl="/api/setProgress";
	var params = {
		"user_name": $("#user_name").val(), 
		"loan_id": $("#loanId").val(), 
		"type": "fast", 
		"tel_number":onlyNumber($("#tel_number").val()), 
		"require_amount" : onlyNumber($("#amount").val()),
		"call_start_time":$("#call_start_time option:selected").val(), 
		"call_end_time":$("#call_end_time option:selected").val()
	};
	var getType="json";
	var contType="application/json; charset=UTF-8";
	ajaxGetData(method , requestUrl , params ,getType , contType , resultData);
}

function resultData (response) {
	if(response.message != "")
		swal("",response.message);
	/*if(response.result){
		window.location.href="step01";
	}*/
	//$('.fast_mask').toggle();
}


/*
 * method : POST , GET
 * getType : HTML JSON XML
 * 
 */
function ajaxGetData(method , requestUrl , params ,getType ,contType , functionObj){
	$.ajax({
		url: requestUrl,
		type: method,
		data: JSON.stringify( params),
		dataType: getType,
		contentType : contType,
		cache: false,
		success: function(result) {
			if (functionObj) {
				functionObj(result);
			} else {
				return result;
			}
		},
		fail: function() {
			swal("","서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요");
		}
	});
 }

function ajaxGetData_get(method , requestUrl , params ,getType ){

	$.ajax({
		url: requestUrl,
		type: method,
		data: params,
		dataType: getType,
		cache: false,
		success: function(result) {
			//alert(result.responseText);
			return result;
		},
		fail: function() {
			swal("","서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요");
		}
	});
 }


//한글만입력
function onlyHan(id , msg)
{
	if(!checkVal(id)){
		return false;
	}
	
	regexp = /[a-z0-9]|[ \[\]{}()<>?|`~!@#$%^&*-_+=,.;:\"'\\]/g;
	v = $("#"+id).val();
	if( regexp.test(v) ) {
		swal("",msg);
		$("#"+id).val(v.replace(regexp,''));
		return false;
	}else{
		return true;
	}
}

//천단위 콤마
function numberWithCommas(x) {
	return x.replace(/[^0-9]/g,'').replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

//숫자만
function onlyNumber(x) {
	return x.replace(/[^0-9]/g,'');
}

//값 확인
function checkVal(id){
	if($("#"+id).val() == ""){
		console.log(id);
		swal("",$("#"+id).attr("data-alert") + " 입력해주세요.");
		return false;
	}else{
		return true;
	}
}

function checkLength(id){
	if($("#"+id).val().length < 10){
		console.log(id);
		console.log($("#"+id).val().length);
		swal("",$("#"+id).attr("data-alert") + " 모두 입력해주세요.");
		return false;
	}else{
		return true;
	}
}
//숫자만 입력
function setNum(obj){
	val=$("#" + obj).val();
	re=/[^0-9]/gi;
	$("#" + obj).val(val.replace(re,""));
}