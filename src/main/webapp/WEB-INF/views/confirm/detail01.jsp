<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="psEntity" value="${map.psEntity }"/>
<c:set var="pEntity" value="${map.pEntity }"/>
<c:set var="efEntityList" value="${map.efEntityList }"/>
<c:set var="efdEntity" value="${map.efdEntity }"/>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
	<%@ include file="/WEB-INF/views/include_web/header_src.jsp"%>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#authRequest").bind("click",function(){
			if($("#bankList option:selected").val()){
				swal("", "입금하실 은행을 선택해 주세요.");
				return;
			}
			
		});
		
		$(".account_holder").keyup(function(){
			$(this).val(onlyNumber($(".account_holder").val()));
		});

		$(".ssn_number").keyup(function(){
			$(this).val(onlyNumber($(".ssn_number").val()));
		});
		
		$("#account_verify_yn").click(function(){
			if($(this).text() != "완료") {
				$("td .al.drop.account_auth_menu").eq(0).trigger("click");
				
			}
		});
		
		$("#btn_account_verify").bind("click",function(){
			$("#account_verify").attr("style","display:block");
		});	
		
    	setClass("call_yn","${psEntity.call_yn}");
    	setClass("eform_yn","${psEntity.eform_yn}");
    	setClass("account_verify_yn","${psEntity.account_verify_yn}");
    	setClass("attach_yn","${psEntity.attach_yn}");
    	
    	setSmsAuthStatus("verify_btn","${psEntity.account_transfer_yn}");
    	setAccountStatus("${psEntity.account_verify_yn}");
    	
    	$("#eform_yn").bind("click",function(){
    		if ($("#eform_yn").text() == "완료") {
    			//swal("", "이미 처리되었습니다.");
    			return;
    		}
    		if($("#account_verify_yn").text() == "완료"){
	    		$.ajax({
	    			url: "/eformUserData?progress_id=${pEntity.id}",
	    			global: false,
	    			type: "GET",
	    			async: false,
	    			success: function (msg) {
	    				if (!msg) {
	    					swal("","먼저 상담사와 전화 상담 진행 후 상담사가 자료를 입력해야만\n전자서식 내용 확인 / 전자 서명 단계를 진행하실 수 있습니다.\n이미 전화 상담을 진행 하셨다면 상담사에게 진행상태 확인을 요청해 주세요.");
	    				} else {
	    					//window.open("/${map.loanId}/confirm/terms_agree?progress_id=${pEntity.id}", "terms_agree", "width=800, height=700, toolbar=no, menubar=no, scrollbars=no, resizable=yes" );
	    					window.location.href="/${map.loanId}/confirm/terms_agree?progress_id=${pEntity.id}";
	    				}
	    			}
	    		})    			
    		}else{
    			swal("","계좌인증이 필요합니다.");	
    		}
    		//window.open("/${map.loanId}/confirm/terms_agree?progress_id=${pEntity.id}", "terms_agree", "width=800, height=700, toolbar=no, menubar=no, scrollbars=no, resizable=yes" ); 
    	});
    	var os = "";
    	//alert(navigator.userAgent.toLocaleLowerCase());
    	if(navigator.userAgent.toLocaleLowerCase().search("iphone") > -1){
    		os = "ios";
    	}else if (navigator.userAgent.toLocaleLowerCase().search("android") > -1){
    		os = "android";
    	}else{
    		os = "other";
    	}
    	
    	$("#attach_yn").bind("click",function(){
    		if ($("#attach_yn").text() == "완료") {
    			//swal("", "이미 처리되었습니다.");
    			return;
    		}
    		if (os != "ios" && os != "android") {
    			swal("", "모바일 전용 서비스입니다. \n휴대폰의 모바일 브라우저에서만 동작합니다.");
    			return;
    		}
  		 	executeApp();
    	});
    	
    	function executeApp() {
			$(".alert_layerpop").attr("style","display:block");
    		swal({
    		  title: "확인",
			  text: "촬영용 앱을 실행합니다.\n앱이 설치되어 있지 않은 경우 앱스토어로 이동합니다.\n촬영 종료 후 페이지를 새로고침 해주시기 바랍니다.",
			  showCancelButton: true,
			  cancelButtonText : "취소",
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "실행",
			  closeOnConfirm: true
			},
    		function(){
	    		var openAt = new Date;
	    		setTimeout(
    		    	function(){
                        newWindow.close();
    		    		//alert(new Date - openAt);
    			 		if (new Date - openAt < 1000) {
    			 			goAppStoreOrPlayStore();
    			 		}
    			 	}, 500);
	    		var url;    		
	    		if (os == "ios") {
	    			url = "goodPaper://progress_id=${pEntity.id}";
	    		} else {
	    			url = "gp://runbyweb?progress_id=${pEntity.id}";
	    		}
                newWindow = window.open(url);
	    		//document.location.href = url;
			});
    		//swal("", "확인");
    		//window.location.reload();
    	}    	
    	
    	
    	function goAppStoreOrPlayStore() {
    		var storeURL ="";
    		if (os == "ios") {
    			storeURL = "https://itunes.apple.com/kr";
    		} else {
    			storeURL = "https://play.google.com/store/apps/details?id=test.svctech.ib.net.paperless_cam";
    		}
    		window.open(storeURL);
    	}
    	
    	$('.request').click(function(){

    		var account_bank_name = $(".bank_select_box option:selected").text();
    		var account_holder_number = $('.account_holder').val();
    		var account_holder_code = $('.bank_select_box').val();
    		var ssn_number = $('.ssn_number').val();
    		var sex = $('.sex').val();
    		
    		
    		if(account_holder_code == '00')
    		{
    			swal("", '입금하실 은행을 선택해 주세요.');	
    			return;
    		}
    		
    		if(account_holder_number.length == 0)
    		{
    			swal("", '계좌번호를 입력해 주세요.');
    			return;
    		}
    		if(ssn_number.length != 6)
    		{
    			swal("", '생년월일 6자리를 입력해 주세요.')	;
    			return;
    		}
    		if(sex == "0")
    		{
    			swal("", "성별을 선택해 주세요.");
    			return;
    		}
    		
    		$.ajax({
    			url: "../inquiry/accountCheck",
    			global: false,
    			type: "POST",
    			data: ({
    				account_holder_number	: account_holder_number,
    				account_holder_code		: account_holder_code,
    				ssn_number				: ssn_number+sex,
    				progress_id				: '${pEntity.id}',
    				user_key 				: '${pEntity.user_key}',
    				account_bank_name		: account_bank_name,
    				loan_id					: '${map.loanId}',
    				id						: '${map.id}'
    			}),
    			dataType: "html",
    			async: false,
    			success: function (msg) {
    				
    				var result = JSON.parse(msg);
    				console.log("transderDeposit response ="+msg);
					swal("", result.rsp_msg);
					
    				if(result.rsp_code == "1"){
    					$("#account_verify").attr("style","display:none");
    					$("#verify_code").removeAttr("disabled");
    					$(".authCodeBox").attr("class","form authCodeBox");
    				}
    			}
    		});
    	});
    	$(".app_upload_reload").on("click",function(){
    		location.reload();
    	});    	
    	//console.log($(".btn").find("bc_gray").text());
    	$("a.btn.bc_gray").bind("click",function(){
    		window.location.href="/${map.loanId}/confirm/list";
    	});
    	$('.smsAuthBtn').click(function(){
    		var smsAuthCode = $('#verify_code').val();
    		
    		if(smsAuthCode.length != 4)
    		{
    			swal("", '인증번호를 입력해 주세요.');
    			return;
    		}
    		
    		$.ajax({
    			url: "../inquiry/smsAuthCodeConfirm",
    			global: false,
    			type: "POST",
    			data: ({
    				smsAuthCode				: smsAuthCode,
    				progress_id				: '${pEntity.id}',
    				user_key 				: '${pEntity.user_key}',
    				loan_id					: '${map.loanId}',
    				id						: '${map.id}'
    			}),
    			dataType: "html",
    			async: false,
    			success: function (msg) {
    				if(msg==1)
    				{
    					swal("", '계좌인증이 완료 되었습니다.');
    					setAccountStatus(1);
    					window.location.reload();
    				}
    				else if(msg == 2)
    				{
    					swal({
    			    		  title: "확인",
    						  text: "인증번호를 5회 잘못 입력 하였습니다.\n인증계좌 등록부터 다시 진행해 주시기 바랍니다.",
    						  showCancelButton: false,
    						  confirmButtonColor: "#DD6B55",
    						  confirmButtonText: "확인",
    						  closeOnConfirm: true
    						},
    			    		function(){
    	    					window.location.reload();
    						});
    				}
    				else
    				{
    					swal("", '인증번호가 일치하지 않습니다.\n확인 후 다시 입력해 주세요.');
    				}
    			}
    		});
    	});
    	
	});
	
	function progressDataUpdate(progressId){
		var params = {"progress_id": progressId};
		$.ajax({
			url: "/api/setUserDataProgressStatus",
			type: "POST",
			data: JSON.stringify( params),
			dataType: "json",
			contentType : "application/json; charset=UTF-8",
			cache: false,
			success: function(response) {

				if(response.message != "") {
					swal("", response.message);
					window.location.reload();
					return;
				}
				if(response.url != "") {
					//alert(response.url);
					window.location.href=response.url;
				} else {
					//alert("else");
					window.location.reload();
				}
					
			},
			fail: function() {
				swal("", "서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요");
			}
		});
	}
	
    function setClass(obj , status){
    	if(status == 0){
    		$("#"+obj).removeAttr("class");
    		$("#"+obj).addClass("progress pg_not");
    		$("#"+obj).text("진행하기");
    	}else if(status == 1){
    		$("#"+obj).removeAttr("class");
    		$("#"+obj).addClass("progress pg_finish");
    		$("#"+obj).text("완료");
    	}else{
    		$("#"+obj).removeAttr("class");
    		$("#"+obj).addClass("progress pg_ing");
    		$("#"+obj).text("진행중");
    	}
    }
    
    function setAccountStatus(status){
    	if(status == 1)
    	{
			//$(".account_auth_confirm_box").removeAttr("hidden");
			$(".account_auth_menu").removeClass("drop");
			$(".account_cont_margin").attr("hidden","");
			$(".account_auth_box").attr("hidden","");
			$(".account_auth_Comment_box").attr("hidden","");
    	}
    }
    function setSmsAuthStatus(attr, status){
    	if(status == 1)
    	{
			$("#account_verify").attr("style","display:none");
			$("#verify_code").removeAttr("disabled");
			$(".authCodeBox").attr("class","form authCodeBox");
    	}
    }
    
    
	</script>
</head>
<body>
<div id="wrap">
	<!-- header -->
	<%@ include file="/WEB-INF/views/include_web/header_top.jsp"%>
	<!-- //header -->
	<!-- gnb -->
	<%@ include file="/WEB-INF/views/include_web/gnb.jsp"%>
	<!-- //gnb -->
	
	<!-- contents -->
	<div id="container">

		<div class="contbox bg">
			<h2 class="stit">대출신청 정보 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:progressDataUpdate('${pEntity.id}');">테스트 진행</a></h2>
			<!-- wrt_table -->
			<div class="wrt_table">
			    <table cellspacing="0" border="1">
			        <caption>대출신청 정보</caption>
			        <colgroup>
			            <col style="width:40%;">
			            <col style="width:auto;">
			        </colgroup>
			        <tbody>
			        <tr class="first">
			            <th scope="row">신청일</th>
			            <td>${psEntity.reg_date }</td>
			        </tr>
			        <tr>
			            <th scope="row">대출금액</th>
			            <td><fmt:formatNumber value="${psEntity.require_amount}" pattern="#,###"/> 만원</td>
			        </tr>
			        <c:if test="${not empty efdEntity.interest_per}">
			        <tr>
			            <th scope="row">금리</th>
			            <td>${efdEntity.interest_per} ${efdEntity.interest_per_memo}</td>
			        </tr>
			        </c:if>
			        <c:if test="${not empty efdEntity.start_date}">
			        <tr>
			            <th scope="row">기간</th>
			            <td>${fn:substring(fn:replace(efdEntity.start_date,"-","."),0, 10)}~${fn:substring(fn:replace(efdEntity.expire_date,"-","."),0, 10)}</td>
			        </tr>
			        </c:if>
			        <!-- tr>
			            <th scope="row">진행상태</th>
			            <td>${psEntity.status_name}</td>
			        </tr-->
			        </tbody>
			    </table>
			</div>
			<!-- //wrt_table -->
		</div>

		<div class="contwbox tline">
			<%-- <h2 class="stit">진행상태&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:progressDataUpdate('${pEntity.id}');">test</a></h2> --%>
			<div></div>
			<!-- lst_table -->
			<div class="lst_table">
			    <table cellspacing="0" border="1">
			        <caption>진행상태</caption>
			        <colgroup>
			            <col style="width:70%;">
			            <col style="width:auto;">
			        </colgroup>
			        <thead>
				        <tr>
				            <th scope="col" class="al">단계</th>
				            <th scope="col">상태</th>
				        </tr>
			        </thead>
			        <tbody>
				        <tr>
				            <td class="al">전화상담</td>
				            <td><span class="progress pg_finish" id="call_yn">완료</span></td>
				        </tr>
				        <tr>
				            <td class="al drop account_auth_menu">입금계좌 등록/인증</td>
				            <td><span class="progress pg_ing" id="account_verify_yn">진행중</span></td>
				        </tr>
				        <!-- 계좌 인증 -->
				        <tr class="drop_cont">
				        	<td colspan="2" class="account_cont_margin">
				        		<dl class="confirm_form01 account_cont account_auth_box">
									<dt>
										<strong>인증 계좌 등록</strong>
										<div class="dt_btn"><button type="button" class="btn bc_purple" id="btn_account_verify">등록</button></div>
									</dt>
									<dd class="form disablebox authCodeBox">
										<input type="text" name="" id="verify_code" disabled="disabled">
										<button type="button" id="verify_btn" class="btn bc_purple smsAuthBtn">인증확인</button>
									</dd>
								</dl>
				        		<ul class="lnc_txt account_auth_Comment_box">
									<li>※ 먼저, 등록 계좌에서 입금 내역을 확인하세요.</li>
									<li>※ 입금자 명의 4자리 코드를 입력해 인증을 완료하세요.</li>
								</ul>
								<dl class="confirm_form01 account_cont account_auth_confirm_box" hidden>
									<dt>정상적으로 등록 계좌를 통해 인증이 처리되었습니다.</li>
								</dl>
				        	</td>
				        </tr>
				        <!-- //계좌 인증 -->
				        <tr>
				            <td class="al">대출 약정서&amp;설명서 전자서명</td>
				            <td><span class="progress pg_not" id="eform_yn">진행하기</span></td>
				        </tr>
				        
				        
				        <tr>
				            <td class="al drop">추가 서류 제출 (전용촬영 APP)</td>
				            <td><span class="progress pg_not" id="attach_yn">재요청</span></td>
				        </tr>
				        <!-- 추가서류제출 -->
					<c:choose>
				        <c:when test="${!empty efEntityList}">
				        <tr class="drop_cont">
				        	<td colspan="2">
				        		<ul class="document_photo">
				        		<c:forEach items="${efEntityList}" var="list">
				        			<li>
				        				<p>${list.name }</p>
				        				<div>
				        					<c:choose>
											<c:when test="${list.upload_yn eq '1'}">	
												<c:choose>			        					
			        							<c:when test="${list.admin_confirm_yn eq '0'}">
			        								<span class="progress pg_ing" id="attach_${list.eform_attach_id}">검토중</span>
			        							</c:when>
			        							<c:when test="${list.admin_confirm_yn eq '1'}">
			        								<span class="progress pg_finish" id="attach_${list.eform_attach_id}">완료</span>
			        							</c:when>
			        							<c:when test="${list.admin_confirm_yn eq '2'}">
			        								<span class="progress pg_not" id="attach_${list.eform_attach_id}">재요청</span>
			        							</c:when>
			        							</c:choose>
			        						</c:when>
			        						<c:otherwise>
				        						<span class="progress pg_not" id="attach_${list.eform_attach_id}">진행하기</span>
				        					</c:otherwise>
				        					</c:choose>
				        				</div>
				        			</li>
				        		</c:forEach>
				        			<!-- 
				        			<li>
				        				<p>등본</p>
				        				<div><span class="progress pg_finish">완료</span></div>
				        			</li>
				        			<li>
				        				<p>등기부등본</p>
				        				<div><span class="progress pg_not">재요청</span></div>
				        			</li>
				        			<li>
				        				<p>집문서</p>
				        				<div><span class="progress pg_not">진행하기</span></div>
				        			</li> -->
				        		</ul>
				        		<ul class="lnc_txt">
									<li>※ 해당 서류 내용을 정확히 확인할 수 있도록 촬영하세요.</li>
								</ul>
				        	</td>
				        </tr>
				        </c:when>
					</c:choose>
				        <!-- //추가서류제출 -->
			        </tbody>
			    </table>
			</div>
			<!-- //lst_table -->

			<ul class="lnc_txt lrm">
				<li>※ 모든 단계를 완료하면, 신청한 대출금액이 입금됩니다.</li>
			</ul>
		</div>

		<!-- div class="btm_btnbox">
			<a href="#" class="btn bc_gray">대출신청확인으로 이동</a>
		</div -->

		

	</div>
	<!-- //contents -->
</div>

<!-- 입금계좌인증 레이어팝업 -->
<div class="layerpop" id="account_verify" style="display:none;">
	<div class="layer_header">
		<strong class="tit">입금계좌인증</strong>
		<button tyle="button" class="layer_close">닫기</button>
	</div>
	<div class="layer_content">
		<div class="contbox bg">
			<ul class="layer_accountadd">
				<li>
					<div class="tit">이름</div>
					<div class="cont">${map.name}</div>
				</li>
				<li>
					<div class="tit">은행</div>
					<div class="cont">
						<select name="" id="" class="select_box bank_select_box">
						    <option value="00">입금할 은행을 선택하세요.</option>
						   	<c:forEach var="item" items="${map.bankList}">
							     <option value="${item.bank_code_std}">${item.bank_name}</option>
							</c:forEach> X
						</select>
					</div>
				</li>
				<li>
					<div class="tit">계좌번호</div>
					<div class="cont"><input type="number" name="" id="" class="account_holder" style="width:100%;ime-mode:disabled" placeholder="계좌번호를 입력하세요." tabindex=1></div> 
				</li>
				<li>
					<div class="tit">생년월일</div>
					<div class="cont"><input type="number" name="" id="" class="ssn_number" style="width:100%;ime-mode:disabled" placeholder="생년월일 6자리 (ex 800521)" tabindex=2></div> 
				</li>
				<li>
					<div class="tit">성별</div>
					<div class="cont">
						<select name="" id="" class="select_box sex">
						    <option value="0">성별을 선택하세요.</option>
						    <option value="1">남자</option>
						    <option value="2">여자</option>
						</select>
					</div>
				</li>
			</ul>
		</div>

		<div class="btm_btnbox02">
			<div href="#" class="btn bc_purple request" tabindex=3>확인</div>
		</div>

		<div class="contbox">
			<p class="account_steptit">입금 계좌 인증 절차</p>
			<ol class="account_steplst">
				<li>신청 완료 후, 대출금이 입금될 은행과 계좌번호를 등록합니다.</li>
				<li>등록한 계좌로 특정금액이 자동 입금이 진행되며, 해당 은행의 뱅킹 서비스를 통해 입금 내역을 확인합니다.</li>
				<li>입금 내역 중 입금자 명에 표기된 코드(영문, 숫자 4자리)를 확인합니다.</li>
				<li>본 서비스에 다시 접속해, 4자리 코드를 계좌 인증 메뉴에 입력 후 계좌 인증 절차를 완료합니다.</li>
			</ol>
		</div>
	</div>
</div>
<!-- //입금계좌인증 레이어팝업 -->

<!-- alert 레이어팝업 -->
<div class="alert_layerpop" style="display:none;">
	<div class="mask_area"></div>
	<div class="layerpop">
		<div class="layer_header">
			<strong class="tit">ABC캐피탈</strong>
		</div>
		<div class="layer_content">
			<div class="contbox">
				대출 서류 제출 완료 후 확인 버튼을 눌러주세요.
			</div>
		</div>
		<div class="btm_btnbox">
			<a href="#" class="btn bc_purple app_upload_reload">확인</a>
		</div>
	</div>
</div>
<!-- //alert 레이어팝업 -->   
   

</body>
</html>