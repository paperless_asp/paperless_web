<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
	<%@ include file="/WEB-INF/views/include_web/header_src.jsp"%>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#auth_res").bind("click",function(){
			
			if($("#tel_number").val() == ""){ 
				swal("","휴대폰 번호를 확인해주십시오.");
				return;
			}
			$.ajax({
    			url: "../inquiry/sendAuthCode",
    			global: false,
    			type: "POST",
    			data: ({
    				loan_id: "${map.loanId}", 
    				tel_number	: $("#tel_number").val(),
    			}),
    			dataType: "html",
    			async: false,
    			success: function (msg) {
    				swal("","인증번호가 전송되었습니다.");
    				$("#auth_confirm").removeClass("bc_gray");
    				$("#auth_confirm").addClass("bc_purple");
    			}
    		});
		});
		
		$("#auth_confirm").bind("click",function(){
			if($("#tel_number").val() == ""){
				swal("","휴대폰 번호를 확인해주십시오.");
				return;
			}
			if($("#auth_number").val() == ""){
				swal("","인증 번호를 확인해주십시오.");
				return;
			}
			
			$.ajax({
    			url: "../inquiry/authCodeConfirm",
    			global: false,
    			type: "POST",
    			data: ({
    				code	: $("#auth_number").val(),
    				tel_number	: $("#tel_number").val(),
    				loan_id : "${map.loanId}",
    			}),
    			dataType: "html",
    			async: false,
    			success: function (msg) {
    				if(msg == 1)
    				{
    					$.blockUI({ 
    						message :
    							"<div class='loading_box' style='display:block;'><div class='mask_area'></div>"+
    							"<div class='loading_cont'><img src='/static/img/loading_1.gif'/></div>"
    					}); 
        		        setTimeout($.unblockUI, 2000); 
        				$("#form1").attr({action:"list",method:"post"}).submit();
        				//makeUserSession();
    				}
    				else if(msg == 2){
    					swal({
  			    		  title: "확인",
  						  text: "인증번호를 5회 잘못 입력 하였습니다.\n다시 진행해 주시기 바랍니다.",
  						  showCancelButton: false,
  						  confirmButtonColor: "#DD6B55",
  						  confirmButtonText: "확인",
  						  closeOnConfirm: true
  						},
  			    		function(){
  	    					window.location.reload();
  						});
    				}
    				else
    				{
    					swal("",'인증번호가 올바르지 않습니다. 다시 한번 확인해 주세요.');	
    				}
    			}
    		});
		});
		
	});
	
	function makeUserSession(){
		//대출신청 저장
		//${map.loanId }
		var method="POST";
		var requestUrl="/api/makeUserSession";
		var params = {
			"loan_id": "${map.loanId}", 
			"tel_number":$("#tel_number").val()
		};
		var getType="json";
		var contType="application/json; charset=UTF-8";
		ajaxGetData(method , requestUrl , params ,getType , contType , resultData);
	}
	
	function resultData(response){
		if(response.message != "")
			swal("",response.message);
		$("#form1").attr({action:"list",method:"post"}).submit();
	}
	</script>
</head>
<body class="bg">
<div id="wrap">
	<!-- header -->
	<%@ include file="/WEB-INF/views/include_web/header_top.jsp"%>
	<!-- //header -->
	<!-- gnb -->
	<%@ include file="/WEB-INF/views/include_web/gnb.jsp"%>
	<!-- //gnb -->
	<form id="form1">
	
	<!-- contents -->
	<div id="container">
		<div class="top_infotxt">
			정보보호를 위해<br>
			<span>휴대폰 인증 진행 후 내역을 조회</span>할 수 있습니다.<br>
			인증번호 요청 후 <span>정확히 입력해 주세요.</span>
		</div>	

		<div class="contbox">
			<dl class="confirm_form01">
				<dt>휴대폰 번호</dt>
				<dd class="form">
					<input type="number" name="tel_number" id="tel_number">
					<button type="button" class="btn bc_purple" id="auth_res">인증번호 요청</button>
				</dd>
			</dl>
			<dl class="confirm_form01 tline">
				<dt>인증번호 입력</dt>
				<dd class="form">
					<input type="number" name="auth_number" id="auth_number">
					<button type="button" class="btn bc_gray" id="auth_confirm">인증확인</button>
				</dd>
			</dl>
			<ul class="lnc_txt">
				<li>※ 인증번호를 수신하지 못할 경우, 인증번호 재발송을 선택하세요.</li>
			</ul>
		</div>

	</div>
	</form>
	<!-- //contents -->
</div>

<!-- loading -->
<div class="loading_box" style="display:none;">
	<div class="mask_area"></div>	
	<div class="loading_cont"><img src="/static/img/loading_1.gif"></div>
</div>
<!-- //loading -->

</body>
</html>