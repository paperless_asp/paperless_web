<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
	<link rel="stylesheet" type="text/css" href="/static/include/css/common.css">
	<!--[if IE]>
	<script type=”text/javascript” src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script type="text/javascript" src="/static/include/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="/static/include/js/common.js"></script>
</head>
<body class="bg">
<div id="wrap">
	<!-- header -->
	<%@ include file="/WEB-INF/views/include_web/header_top.jsp"%>
	<!-- //header -->
	<!-- gnb -->
	<%@ include file="/WEB-INF/views/include_web/gnb.jsp"%>
	<!-- //gnb -->
	
	<!-- contents -->
	<div id="container">
		
		<input type="hidden" class="loanId" value="${map.loanId}"/>
		<!-- lst_table -->
		<div class="lst_table tbfix">
		    <table cellspacing="0" border="1">
		        <caption>공지사항 리스트</caption>
		        <colgroup>
		            <col style="width:10%;">
		            <col style="width:auto;">
		            <col style="width:30%;">
		        </colgroup>
		        <thead>
			        <tr>
			            <th scope="col">NO</th>
			            <th scope="col" class="al">제목</th>
			            <th scope="col">등록일</th>
			        </tr>
		        </thead>
		        <tbody class="noticeList">
		        
		        </tbody>
		    </table>
		</div>
		<!-- //lst_table -->
		

	</div>
	<!-- //contents -->

</div>

<!-- 공지사항 상세보기  레이어팝업 -->
<div class="layerpop" id="Layerpop"style="display:none;">
	<div class="layer_header">
		<strong class="tit">공지사항</strong>
		<button style="button" class="layer_close">닫기</button>
	</div>
	<div class="layer_content">
		<dl class="notice_detailbox">
			<dt>
				<strong id="notice_detail_title">2</strong>
				<span id="notice_detail_date">3</span>
			</dt>
			<dd id="notice_detail_body">4
			</dd>
		</dl>
	</div>
</div>
<!-- //공지사항 상세보기 레이어팝업 -->

</body>
</html>
<script type='text/javascript'>

var idx = 0;
var isFinish = 0;
var isLoading = 0;

$(document).ready(function(){
	
	getNoticeList();
	
});

$(window).scroll(function(){
	if($(window).scrollTop() == $(document).height()-$(window).height() && isLoading == 0 && isFinish == 0){
		isLoading = 1;
		getNoticeList();
	}
})

var _noticeList = new Array();

function getNoticeList(){
	var loanId = $(".loanId").val();
	$.ajax({
		url: "/notice/getNoticeList",
		global: false,
		type: "POST",
		data: ({
			page	: idx,
			loanId	: loanId,
		}),
		dataType: "html",
		async: false,
		success: function (msg) {

			var noticeList = JSON.parse(msg);
			var appendData = "";
			
			if(noticeList.length == 0)
			{
				isFinish = 1;
				return;		
			}
			_noticeList = _noticeList.concat(noticeList);
			
			for(var i=0; i<noticeList.length; i++)
			{	
				var index = i+1+idx;
				var noticeInfo = noticeList[i];
				var sysdate = new Date(noticeInfo.reg_date).toISOString().replace(/T\w.+/g, '');

				var seq = noticeInfo.seq;
				
				appendData = appendData + '<tr><td>'+index+'</td><td class="al txt_cut"><a href="javascript:doDisplay('+index+');">'+noticeInfo.title+'</a></td><td>'+sysdate+'</td></tr>';
			}
			idx = idx+20;
			isLoading = 0;
			$('.noticeList').append(appendData);
		}
	})
}

function doDisplay(index)
{	
	var layerpop = document.getElementById("Layerpop");
	
    if(layerpop.style.display=='none'){

    	var title = document.getElementById("notice_detail_title");
    	var date = document.getElementById("notice_detail_date");
    	var body = document.getElementById("notice_detail_body");
    	
    	var noticeInfo = _noticeList[index-1];
    	
    	title.innerText = noticeInfo.title;
    	date.innerText = new Date(noticeInfo.reg_date).toISOString().replace(/T\w.+/g, '');
    	body.innerText = noticeInfo.body;
    	
    	layerpop.style.display = 'block';
        
        
    }else{
    	layerpop.style.display = 'none';
    }

}

</script>