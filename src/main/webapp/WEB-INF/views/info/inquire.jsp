<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
	<link rel="stylesheet" type="text/css" href="/static/include/css/common.css">
	<!--[if IE]>
	<script type=”text/javascript” src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script type="text/javascript" src="/static/include/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="/static/include/js/common.js"></script>
</head>
<body class="bg">
<div id="wrap">
	<!-- header -->
	<%@ include file="/WEB-INF/views/include_web/header_top.jsp"%>
	<!-- //header -->
	<!-- gnb -->
	<%@ include file="/WEB-INF/views/include_web/gnb.jsp"%>
	<!-- //gnb -->
	
	<!-- contents -->
	<div id="container">
		
		<div class="inquir_box">
			<h2>${map.name} 고객센터</h2>
			<p>서비스 이용 중 문의사항은<br>
			아래로 연락 바랍니다.</p>
			<ul>
				<li>
					<strong>대표번호</strong>
					<a href="tel:${map.telNumber}"><span>Tel. ${map.telNumber}</span></a>
				</li>
				<li>
					<strong>상담센터</strong>
					<a href="tel:${map.callCenter}"><span>Tel. ${map.callCenter}</span></a>
				</li>
			</ul>
		</div>
		

	</div>
	<!-- //contents -->

</div>



</body>
</html>