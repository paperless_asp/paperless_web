<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
	<%@ include file="/WEB-INF/views/include_web/header_src.jsp"%>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#stepStart").bind("click",function(){
			window.location.href="step01";
		});
	});
	</script>
</head>
<body class="bg">
<div id="wrap">
	<!-- header -->
	<%@ include file="/WEB-INF/views/include_web/header_top.jsp"%>
	<!-- //header -->
	<!-- gnb -->
	<%@ include file="/WEB-INF/views/include_web/gnb.jsp"%>
	<!-- //gnb -->
	
	<!-- contents -->
	<div id="container">
		
		<div class="search_infotop">
			<h2>나의 신용 등급을 확인하세요!</h2>
			<p>신용등급 확인 무료이며,<br>
			신용등급에 영향을 미치지 않습니다.</p>
		</div>

		<div class="search_infolst">
			<ul>
				<li class="icon01">전자문서와 서류사진으로 간편하게!</li>
				<li class="icon02">신용등급 별 최저 금리 지원!</li>
				<li class="icon03">최대 3,000만원까지 당일 대출!</li>
				<li class="icon04">최대 60개월까지 자유롭게!</li>
				<li class="icon05">별도 추가 수수료 없이 간편하게!</li>
			</ul>
		</div>

		<p class="search_infobtxt">신용조회 후 바로 대출신청을 진행 할 수 있습니다.</p>
		<div class="btm_btnbox search_infobtn">
			<a href="#" class="btn bc_purple" id="stepStart">신용등급 확인 및 대출 가능한도 조회</a>
		</div>

	</div>
	<!-- //contents -->

	<!-- 빠른 상담신청 -->
	<%@ include file="/WEB-INF/views/include_web/fast_app.jsp"%>
	<!-- //빠른 상담신청 -->
</div>



</body>
</html>