<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import ="java.util.*,java.text.SimpleDateFormat"%>

<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>비대면 대출 신청 서비스</title>
	<%@ include file="/WEB-INF/views/include_web/header_src.jsp"%>
	<script type="text/javascript">
	$(document).ready(function(){
		
		$("#all_agree").bind("click",function(){
			$(".drop_cont").attr("style","display:table-row");
			if($("#all_agree").is(":checked")){
				$("#agree01").prop("checked","checked");
				$("#agree02").prop("checked","checked");
				$("#agree03").prop("checked","checked");
				$("#agree04").prop("checked","checked");
			}else{
				$("#agree01").prop("checked","");
				$("#agree02").prop("checked","");
				$("#agree03").prop("checked","");
				$("#agree04").prop("checked","");
			}
		});
		
		$("input[name='agree01']").click(function(){
			var boo = true;
			$("input[name='agree01']").each(function(k,v){
				if(!$(this).prop("checked")){
					boo = false;
				}
				if(boo)
					$("#all_agree").prop("checked",true);
				else
					$("#all_agree").prop("checked",false);
			});
		});
		
		$(".link").bind("click",function(){
			$("#juminAgreeLayer").attr("style","display:block");
		});
		
		$("#infoAgree").bind("click",function(){
			$("#infoAgreeLayer").attr("style","display:block");
		});
		
		$("#hp_num2").keyup(function(){
			setNum("hp_num2");
		});
		/*
		$("#user_name").on("keyup",function(){
			onlyHan("user_name" , "한글만 가능합니다.");
		});
		*/
		
		/* $("#jumin1").on("keyup",function(){
			setNum(this);
		}); */
		$(this).bind("keyup",function(){
			var boo = true;
			//필수값 체크
			if($("#user_name").val() == "")
				boo = false;
			if($("#hp_num2").val() == "")
				boo = false;
			if($("#require_amount").val() == "")
				boo = false;
			
			if(boo){
				$("#nextStep").removeClass("bc_gray");
				$("#nextStep").addClass("bc_purple");	
			}else{
				$("#nextStep").removeClass("bc_purple");
				$("#nextStep").addClass("bc_gray");
			}
		});
		
		$(this).bind("click",function(){
			var boo = true;
			//필수값 체크
			if($("#user_name").val() == "")
				boo = false;
			if($("#require_amount").val() == "")
				boo = false;
			
			if(boo){
				$("#nextStep").removeClass("bc_gray");
				$("#nextStep").addClass("bc_purple");	
			}else{
				$("#nextStep").removeClass("bc_purple");
				$("#nextStep").addClass("bc_gray");
			}
		});
		
		$("#nextStep").bind("click",function(){

			if($("#user_name").val() == ""){
				swal("","이름을 확인해주십시오.");
				$("#user_name").focus();
				return false;
			}
			
			if($("#hp_num2").val()==""){
				swal("","휴대폰 번호를 확인해주십시오.");
				$("#hp_num2").focus();
				return false;
			}
			
			var require_amount = onlyNumber($("#require_amount").val());
			if((require_amount*1) < ($("#require_amount").attr("data-min")*1)){
				swal("","입력하신 희망 대출금액이 최소금액 보다 낮습니다.");
				$("#amount").focus();
				$("#amount").val("");
				return false;
			}
			
			if((require_amount*1) > ($("#require_amount").attr("data-max")*1)){
				swal("","입력하신 희망 대출금액이 최대금액 보다 높습니다.");
				$("#amount").focus();
				$("#amount").val("");
				return false;
			}
			$("#require_amount").val(require_amount);
			$("#reqPCCForm").removeAttr("onsubmit");
			$("#reqPCCForm").attr({"action":"step02",method:"post"}).submit();
				//window.location.href="step02";
		});
		
		$("#require_amount").keyup(function(){
			$(this).val(numberWithCommas($("#require_amount").val()));
		});
		
		$("#hCheck").bind("click",function(){
			openPCCWindow();	
		});
	});
	
	</script>
	
<script language=javascript>  
<!--
var PCC_window; 

function openPCCWindow(){ 
    var PCC_window = window.open('', 'PCCV3Window', 'width=430, height=560, resizable=1, scrollbars=no, status=0, titlebar=0, toolbar=0, left=300, top=200' );

    if(PCC_window == null){ 
		 alert(" ※ 윈도우 XP SP2 또는 인터넷 익스플로러 7 사용자일 경우에는 \n    화면 상단에 있는 팝업 차단 알림줄을 클릭하여 팝업을 허용해 주시기 바랍니다. \n\n※ MSN,야후,구글 팝업 차단 툴바가 설치된 경우 팝업허용을 해주시기 바랍니다.");
    }

    document.reqPCCForm.action = 'https://pcc.siren24.com/pcc_V3/jsp/pcc_V3_j10.jsp';
    document.reqPCCForm.target = 'PCCV3Window';
    document.reqPCCForm.submit();

	return true;
}	

//-->
</script>	
</head>
<body>
<div id="wrap">
	<!-- header -->
	<%@ include file="/WEB-INF/views/include_web/header_top.jsp"%>
	<!-- //header -->
	
	
	
<%
	Calendar today = Calendar.getInstance();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	String day = sdf.format(today.getTime());
	
	java.util.Random ran = new Random();
	//랜덤 문자 길이
	int numLength = 6;
	String randomStr = "";
	
	for (int i = 0; i < numLength; i++) {
	    //0 ~ 9 랜덤 숫자 생성
	    randomStr += ran.nextInt(10);
	}
	
	//reqNum은 최대 40byte 까지 사용 가능
	String reqNum = day + randomStr;
	String certDate=day;
	
	String id       = "SOQH001";                               // 본인실명확인 회원사 아이디
//	String srvNo    = "001003";                            // 본인실명확인 서비스번호
	String srvNo    = "003001";                            // 본인실명확인 서비스번호
	
	String exVar    = "0000000000000000";                                       // 복호화용 임시필드
	//String retUrl   = request.getRequestURL().toString();                           // 본인실명확인 결과수신 URL
	//String retUrl   = "32http://www.goodpaper.co.kr/abc/search/step01";                           // 본인실명확인 결과수신 URL
	String retUrl   = "32https://www.goodpaper.co.kr/sci/pcc_V3_popup_seed.jsp";                           // 본인실명확인 결과수신 URL
	//String retUrl   = "http://localhost:8080/sanwa/search/step01";                           // 본인실명확인 결과수신 URL
	System.out.println("URL : " + retUrl);
	String certGb	= "H";                           // 본인실명확인 본인확인 인증수단
	String addVar	= "hcheck=y";                           // 본인실명확인 추가 파라메터

	/**
	*
	* reqNum 값은 최종 결과값 복호화를 위한 SecuKey로 활용 되므로 중요합니다.
	* reqNum 은 본인 확인 요청시 항상 새로운 값으로 중복 되지 않게 생성 해야 합니다.
	* 쿠키 또는 Session및 기타 방법을 사용해서 reqNum 값을 
	* pcc_V3_result_seed.jsp에서 가져 올 수 있도록 해야 함.
	* 샘플을 위해서 쿠키를 사용한 것이므로 참고 하시길 바랍니다.
	*
	*/
	Cookie c = new Cookie("reqNum", reqNum);
	//c.setMaxAge(1800);  // <== 필요시 설정(초단위로 설정됩니다)
	response.addCookie(c);
	
	//01. 암호화 모듈 선언
	com.sci.v2.pcc.secu.SciSecuManager seed  = new com.sci.v2.pcc.secu.SciSecuManager();
	
	//02. 1차 암호화
	String encStr = "";
	String reqInfo      = id+"^"+srvNo+"^"+reqNum+"^"+certDate+"^"+certGb+"^"+addVar+"^"+exVar;  // 데이터 암호화
	encStr              = seed.getEncPublic(reqInfo);
	
	//03. 위변조 검증 값 생성
	com.sci.v2.pcc.secu.hmac.SciHmac hmac = new com.sci.v2.pcc.secu.hmac.SciHmac();
	String hmacMsg = hmac.HMacEncriptPublic(encStr);
	
	//03. 2차 암호화
	reqInfo  = seed.getEncPublic(encStr + "^" + hmacMsg + "^" + "0000000000000000");  //2차암호화
	
	
	// 변수 --------------------------------------------------------------------------------
	String retInfo		= "";																// 결과정보
	
	String name			= "";                                                               //성명
	String sex			= "";																//성별
	String birYMD		= "";																//생년월일
	String fgnGbn		= "";																//내외국인 구분값
	
	String di			= "";																//DI
	String ci1			= "";																//CI
	String ci2			= "";																//CI
	String civersion    = "";                                                               //CI Version
	
	String cellNo		= "";																// 핸드폰 번호
	String cellCorp		= "";																// 이동통신사
	
	String result		= "";  
	//복화화용 변수
	String encPara		= "";
	String encMsg		= "";
	String msgChk       = "N";  
	
	//-----------------------------------------------------------------------------------------------------------------

    try{

        // Parameter 수신 --------------------------------------------------------------------
        retInfo = request.getParameter("retInfo");
        

        if (retInfo != null && retInfo.length() > 1) {
        	retInfo  = retInfo.trim();
	        //쿠키값 가져 오기
	        Cookie[] cookies = request.getCookies();
	        String cookiename = "";
	        String cookiereqNum = "";
	    	if(cookies!=null){
	    		for (int i = 0; i < cookies.length; i++){
	    			c = cookies[i];
	    			cookiename = c.getName();
	    			cookiereqNum = c.getValue();
	    			if(cookiename.compareTo("reqNum")==0) break;
	    			
	    			cookiereqNum = null;
	    		}
	    	}
	        
	        // 1. 암호화 모듈 (jar) Loading
	        com.sci.v2.pcc.secu.SciSecuManager sciSecuMg = new com.sci.v2.pcc.secu.SciSecuManager();
	        //쿠키에서 생성한 값을 Key로 생성 한다.
	        retInfo  = sciSecuMg.getDec(retInfo, cookiereqNum);
	
	        // 2.1차 파싱---------------------------------------------------------------
	        String[] aRetInfo1 = retInfo.split("\\^");
	
			encPara  = aRetInfo1[0];         //암호화된 통합 파라미터
	        encMsg   = aRetInfo1[1];    //암호화된 통합 파라미터의 Hash값
			
			String  encMsg2   = sciSecuMg.getMsg(encPara);
				// 3.위/변조 검증 ---------------------------------------------------------------
	        if(encMsg2.equals(encMsg)){
	            msgChk="Y";
	        }
	
			if(msgChk.equals("N")){
				System.out.println("비정상접근입니다.");
			} else {
		        // 복호화 및 위/변조 검증 ---------------------------------------------------------------
				retInfo  = sciSecuMg.getDec(encPara, cookiereqNum);
	
		        String[] aRetInfo = retInfo.split("\\^");
				
		        name		= aRetInfo[0];
				birYMD		= aRetInfo[1];
		        sex			= aRetInfo[2];        
		        fgnGbn		= aRetInfo[3];
		        di			= aRetInfo[4];
		        ci1			= aRetInfo[5];
		        ci2			= aRetInfo[6];
		        civersion	= aRetInfo[7];
		        reqNum		= aRetInfo[8];
		        result		= aRetInfo[9];
		        certGb		= aRetInfo[10];
				cellNo		= aRetInfo[11];
				cellCorp	= aRetInfo[12];
		        certDate	= aRetInfo[13];
				addVar		= aRetInfo[14];
			}
			if (!result.equals("Y")) {
				name = "";
				cellNo = "";
				%>
				<script type="text/javascript">
					swal("","인증에 실패하였습니다.");
				</script>
				<%
			} else {
				%>
				<script type="text/javascript">
					var method="POST";
					var requestUrl="/api/setIdentityAuthInfo";
					var params = {
						"name" : "<%=name%>",
						"phoneNumber" : "<%=cellNo%>",
						"userKey" : "${map.userKey}",
					};
					var getType="json";
					var contType="application/json; charset=UTF-8";
					
					$.ajax({
						url: requestUrl,
						type: method,
						data: JSON.stringify( params),
						dataType: getType,
						contentType : contType,
						cache: false,
						success: function(result) {	
							if(result.result == true){
								swal("","인증되었습니다.");	
								$("#user_name").val("<%=name%>");
								$("#hp_num2").val("<%=cellNo%>");
							}
							else{
								swal("","인증에 실패하였습니다.");
								$("#user_name").val("");
								$("#hp_num2").val("");
							}
						},
						fail: function() {
							swal("","서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요");
							<%
							name = "";
							cellNo = "";
							%>
						}
					});
				</script>
				<%				
			}
        }
    }catch(Exception ex){
        System.out.println("[pcc] Receive Error -"+ex.getMessage());
  }	
%>	
	<!-- gnb -->
	<%@ include file="/WEB-INF/views/include_web/gnb.jsp"%>
	<!-- //gnb -->
	<form id="reqPCCForm" name="reqPCCForm" method="post" action="">
	<input type="hidden" name="reqInfo"     value = "<%=reqInfo%>">
	<input type="hidden" name="retUrl"      value = "<%=retUrl%>">		
	<input type="hidden" name="userKey"     value = "${map.userKey}">	
	<!-- contents -->
	<div id="container">
		<ul class="step_box step01_p">
			<li class="step01 on"><span>본인인증</span></li>
			<li class="step02"><span>개인정보조회동의</span></li> 
			<li class="step03"><span>조회완료</span></li>
		</ul>

		<!-- div class="contwbox bg">
			<h2 class="stit">본인인증</h2>
			<dl class="confirm_btnbox">
				<dt>휴대폰 본인확인</dt>

				<dd><button type="button" class="btn bc_purple" id="hCheck">인증하기</button></dd>
			</dl>
		</div-->
	
		<div class="contwbox bg">
			<h2 class="stit">본인인증</h2>
			<dl class="confirm_btnbox">
				<dt>휴대폰 본인확인</dt>
				<dd><button type="button" class="btn bc_purple" id="hCheck">인증하기</button></dd>
			</dl>
		</div>		

		<div class="contbox tline bg">
			<h2 class="stit">기본정보</h2>
			<!-- ns_table -->
			<div class="ns_table">
			    <table cellspacing="0" border="1">
			        <caption>기본정보</caption>
			        <colgroup>
			            <col style="width:30%;">
			            <col style="width:auto;">
			        </colgroup>
			        <tbody>
			        <tr class="first">
			            <th scope="row">이름</th>
			            <td><input type="text" name="user_name" id="user_name" style="width:100%;opacity:0.4;" value="<%=name%>" readOnly></td>
			            <!-- td><input type="text" name="user_name" id="user_name" style="width:100%;opacity:0.4;" value="<%=name%>" ></td-->
			        </tr>
			        <tr>
			            <th scope="row">휴대폰 번호</th>
			            <td><input type="text" name="hp_num2" id="hp_num2" style="width:100%;opacity:0.4;" maxlength=11  value="<%=cellNo%>" readOnly></td>
			            <!-- td><input type="text" name="hp_num2" id="hp_num2" style="width:100%;opacity:0.4;" maxlength=11  value="<%=cellNo%>" ></td-->
			        </tr>
			        <tr>
			            <th scope="row">대출희망금액</th>
			            <td>
			            	<input type="text" name="require_amount" id="require_amount" style="width:25%;" data-min="${map.loanInfo.min_amount }" data-max="${map.loanInfo.max_amount }">
			            	만원 <span class="cost_num">
			            	(<script language="javascript">
			            		document.write(numberWithCommas("<c:out value="${map.loanInfo.min_amount}"/>"));
			            		document.write("~");
			            		document.write(numberWithCommas("<c:out value="${map.loanInfo.max_amount}"/>"));
			            	</script>만원 가능)</span>
			            </td>
			        </tr>
			        </tbody>
			    </table>
			</div>
			<!-- //ns_table -->
		</div>


		<p class="txt_btmarea">본 신용조회는 개인신용평가에 영향을 미치지 않습니다.</p>
		<div class="btm_btnbox">
			<a href="#" class="btn bc_gray" id="nextStep">다음</a>
		</div>

	</div>
	</form>
	<!-- //contents -->
</div>

<!-- 주민등록번호 도용금지안내 레이어팝업 -->
<div class="layerpop" id="juminAgreeLayer" style="display:none;">
	<div class="layer_header">
		<strong class="tit">주민등록번호 도용금지안내</strong>
		<button tyle="button" class="layer_close">닫기</button>
	</div>
	<div class="layer_content">
		<div class="contbox">
			<p class="layer_txt01">’06.9.25부터 시행(‘06.3.24 공포)된 주민등록법에 따라 주민등록번호의 부정사용에 따른 처벌이 강화 되었으므로 부정사용으로 처벌받지 않도록 주의하시기 바랍니다.</p>
			
			<ul class="layer_lst01">
				<li>3년 이하 징역 또는 1,000만원 이하 벌금</li>
				<li>재물ᆞ재산상 이익을 도모한 경우 뿐만 아니라, 단순 도용도 처벌</li>
			</ul>
		</div>
	</div>
</div>
<!-- //주민등록번호 도용금지안내 레이어팝업 -->

<!-- 개인정보수집 이용 동의(휴대폰본인확인) 레이어팝업 -->
<div class="layerpop" id="infoAgreeLayer" style="display:none;">
	<div class="layer_header">
		<strong class="tit">개인정보수집 이용 동의</strong>
		<button tyle="button" class="layer_close">닫기</button>
	</div>
	<div class="layer_content">
		<div class="contbox">
			<p class="layer_txt01">(주)NICE(이하 “회사”라고 한다)가 제공하는 “본인확인서비스＂는 휴대폰 본인확인과 관련하여 본인으로부터 취득한 개인정보는 “정보통신망이용촉진 및 정보보호등에 관한 법률“ 및“신용정보의 이용 및 보호에 관한 법률”에 따라 본인의동의를 얻어 다음의 목적을 위해 제공 및 이용됩니다.</p>
			
			<p class="layer_txt01">[개인정보의 수집 및 이용 목적]<br>
			“회사”는 생년월일과 휴대폰번호 일치 여부 및 휴대폰 점유 확인과 휴대폰번호 보호 서비스를 안내하기 위한 목적으로 아래의 회사에 다음의 정보를 이용 및 제공합니다.</p>

			<p class="layer_txt01">1. 이용 및 제공 정보<br>
			휴대폰번호, 통신사정보, 생년월일, 성명, 성별, 내외국인정보 </p>
			
			<p class="layer_txt01">2. 제공사<br>
			SKT, KT, LG U+, 드림시큐리티, 스탠다드네트웍스, 인포뱅크, 본인확인서비스 요청 사업자(OO대출)</p>

			<p class="layer_txt01">[개인정보의 보유 및 이용기간]<br>
			“회사”는  이용자의 개인정보를 이용목적이 달성되거나 보유 및 보존기간이 종료하면 해당 정보를 지체없이 파기하며, 별도의 보관을 하지 않습니다. 단,관련 법령 및 회사 방침에 따라 보존하는 목적과 기간은 아래와 같습니다.</p>

			<p class="layer_txt01">[관련법령에 의한 정보보호 사유]<br>
			정보통신망 이용촉진 및 정보보호 등에 관한 법률과 신용정보의 이용 및 보호에 관한 법률 등 관계법령의 규정에 의하여 보존할 필요가 있는 경우 회사는 관계법령에서 정한</p>
		</div>
	</div>
</div>

</body>
</html>