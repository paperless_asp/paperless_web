<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
	<div class="mask_area fast_mask"></div>
	<div class="fast_app">
	<input type="hidden" id="loanId" value="${map.loanId}"/>
		<button type="button" class="fastapp_btn">빠른 상담신청</button>
		<div class="fastapp_contents">
			<p class="fastapp_toptxt">아래 정보 입력 후 전화상담을 예약하세요.<br>
			희망 시간에 맞춰 전문 상담원이 전화드립니다.</p>
			<div class="ns_table">
			    <table cellspacing="0" border="1">
			        <caption>빠른 상담신청</caption>
			        <colgroup>
			            <col style="width:30%;">
			            <col style="width:auto;">
			        </colgroup>
			        <tbody>
			        <tr>
			            <th scope="row">이름</th>
			            <td><input type="text" name="" id="user_name" data-alert="이름을" style="width:100%;"></td>
			        </tr>
			        <tr>
			            <th scope="row">연락처</th>
			            <td><input type="text" name="" id="tel_number" data-alert="연락처를" style="width:100%;"></td>
			        </tr>
			        <tr>
			            <th scope="row">희망대출금액</th>
			            <td>
			            	<input type="text" name="" id="amount" data-alert="희망대출금액을" style="width:20%;" data-min="${map.loanInfo.min_amount }" data-max="${map.loanInfo.max_amount }">
			            	만원 <span class="cost_num">(${map.loanInfo.min_amount }~${map.loanInfo.max_amount }만원 가능)</span>
			            </td>
			        </tr>
			        <tr>
			            <th scope="row">희망상담시간</th>
			            <td>
			            	<select name="" id="call_start_time" style="width:20%">
							    <c:forEach begin="1" end="24" step="1" var="index">
								    <c:choose>
								    	<c:when test="${index < 10 }">
								    		<option value="0${index}" <c:if test="${index == 9}">selected</c:if>>0${index}</option>
								    	</c:when>
								    	<c:otherwise>
								    		<option value="${index}" <c:if test="${index == 9}">selected</c:if>>${index}</option>
								    	</c:otherwise>
								    </c:choose>
							    </c:forEach>
							</select>
							~
							<select name="" id="call_end_time" style="width:20%">
							    <c:forEach begin="1" end="24" step="1" var="index">
								    <c:choose>
								    	<c:when test="${index < 10 }">
								    		<option value="0${index}" <c:if test="${index == 18}">selected</c:if>>0${index}</option>
								    	</c:when>
								    	<c:otherwise>
								    		<option value="${index}" <c:if test="${index == 18}">selected</c:if>>${index}</option>
								    	</c:otherwise>
								    </c:choose>
							    </c:forEach>
							</select>
							<span class="form_btn">
							    <input type="checkbox" name="fast_check" id="fast_check" >
							    <label for="fast_check">상관없음</label>
							</span>
			            </td>
			        </tr>
			        </tbody> 
			    </table>
			</div>
			<div class="btm_btnbox">
				<a href="#" class="btn bc_purple" id="faseBtn">전화상담예약</a>
			</div>
		</div>
	</div>