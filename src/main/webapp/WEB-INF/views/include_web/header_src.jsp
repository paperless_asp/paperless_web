<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@page errorPage="/error.jsp" %>
<link rel="stylesheet" type="text/css" href="/static/include/css/common.css">
<link rel="stylesheet" type="text/css" href="/static/include/css/sweetalert.css">
<!--[if IE]>
<script type=”text/javascript” src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="/static/include/js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="/static/include/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="/static/include/js/common.js"></script>
<script type="text/javascript" src="/static/include/js/page-common.js"></script>
<script type="text/javascript" src="/static/include/js/sweetalert-dev.js"></script>

<META http-equiv="Expires" content="-1"> 
<META http-equiv="Pragma" content="no-cache"> 
<META http-equiv="Cache-Control" content="No-Cache"> 