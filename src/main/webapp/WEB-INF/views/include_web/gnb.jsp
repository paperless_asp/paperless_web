<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<div class="gnb_mask"></div>
<nav id="gnb">
	<ul>
		<li><a href="/${map.loanId }/search/info" class="menu01">대출신청</a></li>
		<li><a href="/${map.loanId }/confirm/info" class="menu02">대출신청내역 조회</a></li>
		<li class="line"><span></span></li>
		<li><a href="/${map.loanId}/info/inquire">고객문의</a></li>
		<li><a href="/${map.loanId}/info/notice">공지사항</a></li>
		<!-- li><a href="http://devchat.ibizplus.co.kr/example/start_cstalk.html" target="_blank">상담톡</a></li -->
	</ul>
</nav>