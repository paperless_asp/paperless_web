﻿<%@page import="com.clipsoft.clipreport.oof.OOFFile"%>
<%@page import="com.clipsoft.clipreport.oof.OOFDocument"%>
<%@page import="com.clipsoft.clipreport.oof.connection.*"%>
<%@page import="java.io.File"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.lang.reflect.Field"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.clipsoft.clipreport.server.service.ReportUtil"%>
<%@page import="org.springframework.web.context.WebApplicationContext"
	import="org.springframework.web.context.support.WebApplicationContextUtils"
	import="net.ib.paperless.spring.controller.TSARestController"    
	import="net.ib.paperless.spring.domain.EformUserData"    
%>
<%@page import="java.util.Enumeration"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%!
	public static Map ConverObjectToMap(Object obj){
		try {
			//Field[] fields = obj.getClass().getFields(); //private field는 나오지 않음.
			Field[] fields = obj.getClass().getDeclaredFields();
			Map resultMap = new HashMap();
			for(int i=0; i<=fields.length-1;i++){
				fields[i].setAccessible(true);
				resultMap.put(fields[i].getName(), fields[i].get(obj));
			}
			return resultMap;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	};
%>
<% 
	String progress_id = request.getParameter("progress_id");
	String url = "/eformUserData?progress_id=" +progress_id;
	 
	ServletContext servletContext = this.getServletContext();
	WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);

	TSARestController tsaRestController = (TSARestController)wac.getBean("tsaRestController");
	HashMap<String, Object> userDataMap = tsaRestController.getEformUserData(progress_id, request);
	System.out.println(userDataMap);
	JSONObject json = new JSONObject();
	if (userDataMap != null) {
		for (String key : userDataMap.keySet()) {	
		EformUserData userData = (EformUserData)userDataMap.get(key);
			json.put(key, ConverObjectToMap(userData));
		}
	}
	
	String str = json.toString();
	System.out.println("------------------");
	System.out.println(str);
	System.out.println("------------------");
    
	String requestMethod = request.getMethod();
	Enumeration enu = request.getParameterNames();
	String sNames = "";
	
	
	OOFDocument oof = OOFDocument.newOOF();
	OOFFile file = oof.addFile("crfe.root", "%root%/crf/ABC-EFORM1.crfe");
	System.out.println(file.getPath() + ":" + "%root%/crf/ABC-EFORM1.crfe");
	
	//String str = request.getParameter("userdata");
	
	OOFConnectionMemo connectionMemo = oof.addConnectionMemo("*",str);
	connectionMemo.addContentParamJSON("*","utf-8","{%dataset.json.root%}");


//file.addField("name","jspark");
//file.addField("tel","01066376497");


%><%@include file="Property.jsp"%><%
String resultKey =  ReportUtil.createEForm(request, oof, "false", "false", request.getRemoteAddr(), propertyPath);
System.out.println("progress_id : " + progress_id);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>EForm</title>
<meta name="viewport" content="width=800, user-scalable=no">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="./css/clipreport.css">
<link rel="stylesheet" type="text/css" href="./css/eform.css">
<link rel="stylesheet" type="text/css" href="./css/UserConfig.css">
<link rel="stylesheet" type="text/css" href="./css/font.css">
<link rel="stylesheet" type="text/css" href="/static/include/css/sweetalert.css">

<script type='text/javascript' src='./js/jquery-1.11.1.js'></script>
<script type='text/javascript' src='./js/clipreport.js?ver=1.0'></script>
<script type='text/javascript' src='./js/UserConfig.js'></script>
<script type="text/javascript" src="/static/include/js/sweetalert-dev.js"></script>
<script type='text/javascript'>
var urlPath = document.location.protocol + "//" + document.location.host;

function chkData() {
	html2xml('targetDiv1');
}
function html2xml(divPath){
    var eformkey = "<%=resultKey%>";
	var eform = createImportJSPEForm(urlPath + "/eform/Clip.jsp", eformkey, document.getElementById(divPath));
	var file_name = "<%=progress_id%>";
	/*
	eform.setSignOffset(100, 100);
	eform.setSignStaticPosition("50px","50%");
	eform.setNecessaryEnabled(true);
	eform.setEndSaveButtonEvent(function (){
	alert(JSON.stringify(eform.getEFormData()));
	});
	*/
	
	eform.setEndReportEvent(function(){ 
		userEFormEvent(eform);
	});

	/* 입력값 json으로 가져오기
	eform.setEndSaveButtonEvent(function (){
		alert(JSON.stringify(eform.getEFormData()));
	});
	*/

	
	//서버에 PDF 저장하기
	eform.setEndSaveButtonEvent(function (){
		var param = "report_key=" + eform.getReportKey();
		param +="&file_name=" + file_name;
		
		// PDF 생성
		objHttpClient = new HttpClient();
		var strResult = objHttpClient.send(urlPath + '/eform/exportForPartPDF.jsp', param, false, null);
		//alert("결과코드입니다:" + strResult);
		
		// TSA 인증 Signed PDF 생성
		tsaHttpClient = new HttpClient();
		var tsaUrl = urlPath + '/tsa?progress_id=' + file_name;
		var tsaResult = tsaHttpClient.send(tsaUrl, null, false, null);
		swal("", '신청서 작성이 완료되었습니다.');
		window.opener.location.href = opener.location.href.substring(0, opener.location.href.lastIndexOf("/")+1) + "detail01?id=" + file_name;
		window.close();
	});
	eform.view();
}

function userEFormEvent(eform){
	var inputGroup = eform.findGroup("Inputbox1");
	var checkGroup = eform.findGroup("inputcheck1");
	var radioGroup = eform.findGroup("inputradio1");
	var signGroup = eform.findGroup("clipsign1");
	
	var checkControl = checkGroup.getControlList()[0];
	var inputControl = inputGroup.getControlList()[0];
	
	checkControl.onCheckedEvent(function(){
		inputControl.setValue("checked");
		
	});
	checkControl.onUnCheckedEvent(function(){
		inputControl.setValue("unChecked");
	});		
}

function getUserData() {

}

function hashCode (str){
    var hash = 0;
    if (str.length == 0) return hash;
    for (i = 0; i < str.length; i++) {
        char = str.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}
</script>
</head>
<body onload="chkData()">
<div id='targetDiv1' style='position:absolute;top:5px;left:5px;right:5px;bottom:5px;'></div>
</body>
</html>
