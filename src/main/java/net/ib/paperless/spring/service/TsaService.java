package net.ib.paperless.spring.service;

import java.io.File;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.dreamsecurity.common.exception.DTException;
import com.dreamsecurity.pdfsigner.PDFSigner;
import com.dreamsecurity.pdfsigner.PDFSignerArg;

import net.ib.paperless.spring.domain.EformUserData;

@Service
public class TsaService {
	private static final Logger logger = LoggerFactory.getLogger(TsaService.class);
	// TSA 인증 금결원 URL
	private final String kftc_url_test = "http://203.233.91.234:7780/signedReq";	// 개발
	private final String kftc_url_real = "http://203.233.91.181:7780/signedReq";	// 운영

	
//	private final String org_pdf_base_path = "c:\\Work\\";
	
//	@Value("${tempfile.location}")
//	private String org_pdf_base_path = ;

	@Value("${deploy.server}")
	private String deployServer;
	
	@Autowired
	ServletContext servletContext;	
	
	public boolean timestampCertificate(String file_name) {
		try {
			String org_pdf_base_path = servletContext.getRealPath("/org_pdf");
			logger.info("TEMP FILE Location [{}]", org_pdf_base_path);
			PDFSigner pdfSigner = new PDFSigner();	
			//config 파일 경로  설정(was에서 사용시 절대경로)
			PDFSignerArg pdfSingerArg = new PDFSignerArg();
			
//			String propertyPath  =  File.separator +  "WEB-INF" + File.separator + "lib" + File.separator + "pdfsigner.properties";
			String propName = "pdfsigner.properties";
			if (deployServer.equals("dev")) {
				propName = "pdfsigner-dev.properties";
			} else if (deployServer.equals("initech_dev")) {
				propName = "pdfsigner-initech-dev.properties";
			}
			String propertyPath  =  servletContext.getRealPath("/WEB-INF/lib") + File.separator + propName;
			pdfSingerArg.setPropertyPath(propertyPath);
			
			pdfSigner.init(pdfSingerArg);
			
			//시점확인 토큰 서버 URL설정
			//금결원 개발
			pdfSigner.setURL(kftc_url_test);
//			pdfSigner.setURL(kftc_url_real);
			
			//시도횟수 설정 : 기본 3
			pdfSigner.setTryCount(1);
			//connect timeout : 기본 10000ms
			pdfSigner.setConTimeOut(10000);
			//read timeout : 기본 15000ms
			pdfSigner.setReadTimeOut(15000);
			
			//타임스탬프 발급 실행(입력파일, 출력파일)
			String abs_org_file = org_pdf_base_path + File.separator + file_name + ".pdf";
			System.out.println(new File(abs_org_file).getAbsolutePath());;
			logger.info("org file : {}", abs_org_file);
			String abs_signed_file = org_pdf_base_path + File.separator + file_name +"_signed.pdf";
			logger.info("signed file : {}", abs_signed_file);
			pdfSigner.Sign(abs_org_file, abs_signed_file);
		}catch (DTException e) {
			//에러 발생시 에러 정보 출력
			logger.error("에러코드 : {}", e.getLastError());
			logger.error("에러메시지 : {}", e.getMessage());
			logger.error("", e);
			return false;
		} catch (Exception e) {
			logger.error("", e);
			return false;
		} 
		return true;
	}
}
