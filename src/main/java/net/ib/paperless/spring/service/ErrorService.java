package net.ib.paperless.spring.service;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Service;

@Service
public class ErrorService {
	   public int getHttpStatusCode(final HttpServletRequest request){
		   return (int) request.getAttribute("javax.servlet.error.status_code");
	   }
}
