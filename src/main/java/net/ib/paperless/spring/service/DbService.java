package net.ib.paperless.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.ib.paperless.spring.repository.DbRepository;

@Service
public class DbService {
 
    @Autowired
    DbRepository dbMapper;
 
    /* select dual */
    public String getDual() throws Exception{
        return dbMapper.getDual();
    }
 
}