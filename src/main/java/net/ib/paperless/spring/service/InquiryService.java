package net.ib.paperless.spring.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.clipsoft.org.json.simple.JSONObject;

import net.ib.paperless.spring.common.DateUtil;
import net.ib.paperless.spring.common.FileFtpHandler;
import net.ib.paperless.spring.common.UUIDs;
import net.ib.paperless.spring.domain.FtpFile;
import net.ib.paperless.spring.domain.LoanInfo;
import net.ib.paperless.spring.domain.Progress;
import net.ib.paperless.spring.domain.ProgressStatus;
import net.ib.paperless.spring.domain.SmsAuthCodeInfo;
import net.ib.paperless.spring.repository.InquiryRepository;
import net.ib.paperless.spring.repository.ProgressRepository;

@Service
public class InquiryService {

	private String localPath = "C:\\temp\\";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(InquiryService.class);

	@Autowired
	InquiryRepository inquiryMapper;
	
	@Autowired
	ProgressRepository progressMapper;
	// @Value("#{test}") private String tempDir;

	@Autowired
	ProgressService progressService;

	/**
	 * @param phoneNumber
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public Map<String,Object> setLoanUserInfo(Map<String, Object> map){
		Map<String, Object> retMap = new HashMap<String, Object>();
		try{
			if (map.get("tel_number")==null || map.get("loan_id")==null || map.get("require_amount")==null
					|| map.get("call_start_time")==null || map.get("call_end_time")==null || map.get("user_name")==null) {
				retMap.put("result",false);
				retMap.put("msg","parameters error.");
				return retMap;
			}
			//String localPath = "C:\\temp\\";
			String telNumber = map.get("tel_number").toString();
			String currentDate = DateUtil.getCurrentDateString();
			String loanId = map.get("loan_id").toString();
			String userName = map.get("user_name").toString();
			String callStartTime = map.get("call_start_time").toString();
			String callEndTime = map.get("call_end_time").toString();
			String uuids = UUIDs.createNameUUID(telNumber.getBytes()).toString();
			String requireAmount =  map.get("require_amount").toString();

			System.out.println("loanId : "+ loanId);
			LoanInfo loanInfoVo = loanInfoSelect(loanId);
			System.out.println("loanInfoVo : "+ loanInfoVo);
			if(loanInfoVo == null){
				retMap.put("result",false);
				retMap.put("msg","loanInfo Data not find.");
				return retMap;
			}

			//progress 생성
			Progress progressEntity = new Progress();
			progressEntity.setLoan_id(loanId);
			progressEntity.setEform_id("1");
			progressEntity.setUser_name(userName.substring(0,2));
			progressEntity.setTel_number(telNumber.substring(telNumber.length()-4,telNumber.length()));
			progressEntity.setRequire_amount(requireAmount);
			//progressEntity.setAdmin_id("");
			progressEntity.setProgress_status(1);
			progressEntity.setUser_key(uuids);
			progressEntity.setCall_start_time(callStartTime);
			progressEntity.setCall_end_time(callEndTime);
			
			if(progressMapper.progressInsert(progressEntity) == 0){
				retMap.put("result",false);
				retMap.put("msg"," failed make progress.");
				return retMap;
			}

			ProgressStatus progressStatusEntity = new ProgressStatus();
			progressStatusEntity.setProgress_id(progressEntity.getLoan_id() + progressEntity.getSeq());
			if(progressMapper.progressStatusInsert(progressStatusEntity) == 0){
				retMap.put("result",false);
				retMap.put("msg"," failed make progressStatus.");
				return retMap;
			}
			
			// json 파일 생성
			JSONObject obj = new JSONObject();
			// 데이터세팅
			obj.put("progress_id", progressStatusEntity.getProgress_id());
			obj.put("name", userName);
			obj.put("tel", telNumber);
			obj.put("reg", currentDate);
			obj.put("jumin", "");
			obj.put("bank", "");
			obj.put("account", "");
			// 로컬 파일생성
			//String path = localPath + uuids + ".json";

			FtpFile ftpFileVo = new FtpFile();
			ftpFileVo.setFtpIp(loanInfoVo.getFtp_ip());
			ftpFileVo.setFtpFilePath(loanInfoVo.getFtp_base_pass() + "/" + currentDate);
			ftpFileVo.setFileName(uuids + "_" + progressEntity.getSeq() + ".json");
			ftpFileVo.setFtpFileFullPath(ftpFileVo.getFtpFilePath() + "/" + ftpFileVo.getFileName());
			ftpFileVo.setFtpId(loanInfoVo.getFtp_id());
			ftpFileVo.setFtpPwd(loanInfoVo.getFtp_pwd());
			ftpFileVo.setLocalPath(localPath + uuids + ".json");
			ftpFileVo.setFtpPort(loanInfoVo.getFtp_port());
			ftpFileVo.setFtpFileType("local");
			
			if(!FileFtpHandler.jsonFileLocalMake(obj, ftpFileVo.getLocalPath())){
				retMap.put("result",false);
				retMap.put("msg"," failed make json file.");
				return retMap;
			}
			
			
			if(FileFtpHandler.fileUploader(ftpFileVo)){
				//FileFtpHandler.ftpReadData();
				retMap.put("result",true);
//				retMap.put("msg","모바일 대출신청 요청이 완료되었습니다.");
				retMap.put("msg","빠른 상담신청이 완료되었습니다.");
				return retMap;	
			}else{
				retMap.put("result",false);
				retMap.put("msg"," failed upload file.");
				return retMap;
			}

		}catch(Exception e){
			retMap.put("result",false);
			retMap.put("msg","입력하신 자료가 요건에 맞지 않습니다.");
			return retMap;
		}
	}
	public Map<String,Object> setAuthCode(SmsAuthCodeInfo codeInfo){
			
			Map<String, Object> retMap = new HashMap<String, Object>();
		
			if(inquiryMapper.authCodeInsert(codeInfo) == 0){
				retMap.put("result",false);
				retMap.put("msg"," failed insert smsCode");
				return retMap;
			}
			
			retMap.put("result",true);
			retMap.put("msg"," success insert smsCode");
			return retMap;
	}

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, Object> setAccountAuthInfo(String id, String loan_id, String user_key, Map<String, Object> accountInfo)
	{
		Map<String, Object> retMap = new HashMap<String, Object>();
		
		String currentDate = DateUtil.getCurrentDateString();
		LoanInfo loanInfoVo = inquiryMapper.loanInfoSelect(loan_id);
		
		System.out.println("loanInfoVo : "+ loanInfoVo);
		if(loanInfoVo == null){
			retMap.put("result",false);
			retMap.put("msg","loanInfo Data not find.");
			return retMap;
		}

		
		JSONObject obj = new JSONObject();
		Map<String,Object> jMap = new HashMap<String,Object>();
		
		HashMap<String, String> pMap = new HashMap<String, String>();
		pMap.put("user_key", user_key);
		pMap.put("loanId", loan_id);
		pMap.put("id", id);
		
		Progress progressVo = progressService.selectOneProgressListByUserKey(pMap);

		if(progressVo != null){
			jMap = getFtpJsonData(loan_id , user_key , progressVo.getSeq(),progressVo.getReg_date());
		}


		 Set set1 = jMap.keySet();
		 Iterator iter1 = set1.iterator();

		 while (iter1.hasNext()) {
			 String key = (String) iter1.next();
			 obj.put(key, jMap.get(key));
		 }
		 
		 Set set2 = accountInfo.keySet();
		 Iterator iter2 = set2.iterator();

		 while (iter2.hasNext()) {
			 String key = (String) iter2.next();
			 obj.put(key, accountInfo.get(key));
		 }
		
		FtpFile ftpFileVo = new FtpFile();
		ftpFileVo.setFtpIp(loanInfoVo.getFtp_ip());
		ftpFileVo.setFtpFilePath(loanInfoVo.getFtp_base_pass() + "/" + progressVo.getReg_date().replaceAll("-", "").substring(0,8));
		ftpFileVo.setFileName(user_key + "_" + progressVo.getSeq() + ".json");
		ftpFileVo.setFtpFileFullPath(ftpFileVo.getFtpFilePath() + "/" + ftpFileVo.getFileName());
		ftpFileVo.setFtpId(loanInfoVo.getFtp_id());
		ftpFileVo.setFtpPwd(loanInfoVo.getFtp_pwd());
		ftpFileVo.setLocalPath(localPath + user_key + ".json");
		ftpFileVo.setFtpPort(loanInfoVo.getFtp_port());
		ftpFileVo.setFtpFileType("local");

		System.out.println("obj : " + obj);
		System.out.println("getFtpIp : " + ftpFileVo.getFtpIp());
		System.out.println("getFtpFileFullPath : " + ftpFileVo.getFtpFileFullPath());
		System.out.println("getFileName : " + ftpFileVo.getFileName());
		System.out.println("getLocalPath : " + ftpFileVo.getLocalPath());

		System.out.println("obj : " + obj);
		if(!FileFtpHandler.jsonFileLocalMake(obj, ftpFileVo.getLocalPath())){
			retMap.put("result",false);
			retMap.put("msg"," failed make json file.");
			return retMap;
		}
		
		
		if(FileFtpHandler.fileUploader(ftpFileVo)){
			retMap.put("result",true);
			retMap.put("file_path =", loanInfoVo.getFtp_base_pass() + "/" + progressVo.getReg_date().replaceAll("-", "").substring(0,8));
			retMap.put("msg","등록되었습니다.");
			return retMap;
		}else{
			retMap.put("result",false);
			retMap.put("msg"," failed upload file.");
			return retMap;
		}
	}

	public Map<String,Object> getFtpJsonData(String loanId , String uuids , int seq , String regDate){

		LoanInfo loanInfoVo = inquiryMapper.loanInfoSelect(loanId);
		System.out.println("loanInfoVo : "+  loanInfoVo.getFtp_ip()+":"+loanInfoVo.getFtp_port() + " " + regDate);
		
		if(loanInfoVo != null){
			FtpFile ftpFileVo = new FtpFile();
			ftpFileVo.setFtpIp(loanInfoVo.getFtp_ip());
			ftpFileVo.setFtpFilePath(loanInfoVo.getFtp_base_pass() + "/" + regDate.replaceAll("-", "").substring(0,8));
			ftpFileVo.setFileName(uuids + "_" + seq + ".json");
			ftpFileVo.setFtpFileFullPath(ftpFileVo.getFtpFilePath() + "/" + ftpFileVo.getFileName());
			ftpFileVo.setFtpId(loanInfoVo.getFtp_id());
			ftpFileVo.setFtpPwd(loanInfoVo.getFtp_pwd());
			ftpFileVo.setLocalPath(localPath + uuids + ".json");
			ftpFileVo.setFtpPort(loanInfoVo.getFtp_port());
			System.out.println("ftpFileVo : "+ ftpFileVo.getFtpFileFullPath());
			
			return FileFtpHandler.ftpReadData(ftpFileVo);
		}
		return null;
	}


	public LoanInfo loanInfoSelect(String loanId) {
		return inquiryMapper.loanInfoSelect(loanId);
	}
	
	public Map<String,Object> setSmsAuthCode(SmsAuthCodeInfo codeInfo){
		
		Map<String, Object> retMap = new HashMap<String, Object>();
	
		if(inquiryMapper.smsAuthCodeInsert(codeInfo) == 0){
			retMap.put("result",false);
			retMap.put("msg"," failed insert smsCode");
			return retMap;
		}
		
		retMap.put("result",true);
		retMap.put("msg"," success insert smsCode");
		return retMap;
	}

	public SmsAuthCodeInfo authCodeSelect(SmsAuthCodeInfo codeInfo) {
		return inquiryMapper.authCodeSelect(codeInfo);
	}
	
	public int authCodeDelete(SmsAuthCodeInfo codeInfo){
		return inquiryMapper.authCodeDelete(codeInfo);
	}
	
	public int smsAuthCodeUpdate(String progress_id){
		return inquiryMapper.smsAuthCodeUpdate(progress_id);
	}
	
	public int authCodeUpdate(SmsAuthCodeInfo codeInfo){
		return inquiryMapper.authCodeUpdate(codeInfo);
	}
	
	public SmsAuthCodeInfo smsAuthCodeSelect(String progress_id) {
		return inquiryMapper.smsAuthCodeSelect(progress_id);
	}
	
	public int smsAuthCodeDelete(String progress_id){
		return inquiryMapper.smsAuthCodeDelete(progress_id);
	}
	
	public int sendSmsStatusReset(String progress_id){
		return inquiryMapper.sendSmsStatusReset(progress_id);
	}
	
	public int compareAuthCodeStatusUpdate(String progress_id) {
		return inquiryMapper.compareAuthCodeStatusUpdate(progress_id);
	}
	
	public int appInfoCheckSelectOne(Map map){
		return inquiryMapper.appInfoCheckSelectOne(map);
	}
}