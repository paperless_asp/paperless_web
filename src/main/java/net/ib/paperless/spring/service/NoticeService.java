package net.ib.paperless.spring.service;
import java.util.List;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import net.ib.paperless.spring.domain.NoticeInfo;
import net.ib.paperless.spring.repository.NoticeRepository;

@Service
public class NoticeService {
	@Autowired
	NoticeRepository noticeMapper;
	
	@Transactional
	public List<NoticeInfo> getNoticeList(Map<String, Object> map){
		
		List<NoticeInfo> resultMap = noticeMapper.NoticeListSelect((String)map.get("loanId"), (String)map.get("page"));
		return resultMap;
	}
	
}