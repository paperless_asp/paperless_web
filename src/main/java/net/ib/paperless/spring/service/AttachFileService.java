package net.ib.paperless.spring.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import net.ib.paperless.spring.common.FileFtpHandler;
import net.ib.paperless.spring.domain.EformAttach;
import net.ib.paperless.spring.domain.FtpFile;
import net.ib.paperless.spring.domain.LoanInfo;
import net.ib.paperless.spring.domain.ProgressAttach;
import net.ib.paperless.spring.repository.AttachFileRepository;
import net.ib.paperless.spring.repository.InquiryRepository;
import net.ib.paperless.spring.repository.ProgressRepository;

/**
 * 앱에서 등록하는 첨부서류
 * @author jspark
 * 
 */
@Service
public class AttachFileService {
	@Autowired
	AttachFileRepository attachFileRepository;
	@Autowired
	InquiryRepository inquiryRepository;
	@Autowired
	ProgressRepository progressRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(AttachFileService.class);

	public List<EformAttach> eFormAttachSelectByProgressId(String progressId){
		return attachFileRepository.eFormAttachSelectByProgressId(progressId);
	}

	public Boolean eFormAttachFileSave(String progressId , int attachId , MultipartFile attach){
		boolean ret = false;
		HashMap<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("attachId", attachId);
		paramsMap.put("progressId", progressId);
		EformAttach eFormAttachVo = attachFileRepository.eFormAttachSelectByAttachId(paramsMap);
		LoanInfo loanInfoVo = inquiryRepository.loanInfoSelect(eFormAttachVo.getLoan_id());			
		if(loanInfoVo != null && eFormAttachVo != null){
			FtpFile ftpFileVo = new FtpFile();
			ftpFileVo.setFtpIp(loanInfoVo.getFtp_ip());
			ftpFileVo.setFtpFilePath(loanInfoVo.getFtp_base_pass() + "/" + eFormAttachVo.getReg_date() +"/out");
			//ftpFileVo.setFileName(attach.getOriginalFilename());
			ftpFileVo.setFileName(progressId+"_"+attachId);
			ftpFileVo.setFtpFileFullPath(ftpFileVo.getFtpFilePath() + "/" + ftpFileVo.getFileName());
			ftpFileVo.setFtpId(loanInfoVo.getFtp_id());
			ftpFileVo.setFtpPwd(loanInfoVo.getFtp_pwd());
			//ftpFileVo.setLocalPath("");
			ftpFileVo.setFtpPort(loanInfoVo.getFtp_port());
			ftpFileVo.setFtpFileType("multipart");
			ftpFileVo.setFile(attach);
			ret = FileFtpHandler.fileUploader(ftpFileVo);
			if (ret) { 
				// 업로드 상태 반영
				ProgressAttach progressAttach = new ProgressAttach();
				progressAttach.setProgress_id(progressId);
				progressAttach.setEform_attach_id(String.valueOf(attachId));
				progressAttach.setPath("");
				progressAttach.setUpload_yn(1);
				progressAttach.setTransfer_yn(1);
//				progressAttach.setAdmin_confirm_yn(0);
				progressAttach.setAdmin_confirm_yn(1);	// 데모를 위하여 임시로 상담사 검토 완료로 변경
				int cnt = attachFileRepository.progressAttachInsert(progressAttach);
				ret = true;
				
				List<EformAttach> list = attachFileRepository.eFormAttachSelectByProgressId(progressId);
				
				if(!list.isEmpty()){
					boolean attachYnBoo = true;
					for(EformAttach entity : list){
						if(entity.getUpload_yn() != 1)
							attachYnBoo = false;
					}
					if(attachYnBoo){
						Map<String,Object> m = new HashMap<String,Object>();
						m.put("attach_yn", 1);
						m.put("progressId", progressId);
						progressRepository.progressStatusUpdate(m);
					}
				}
			}
		}
		return ret;
	}
}
