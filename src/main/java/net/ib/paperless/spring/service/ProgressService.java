package net.ib.paperless.spring.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import net.ib.paperless.spring.domain.EformAttach;
import net.ib.paperless.spring.domain.EformUserData;
import net.ib.paperless.spring.domain.IdentityAuthInfo;
import net.ib.paperless.spring.domain.Progress;
import net.ib.paperless.spring.domain.ProgressStatus;
import net.ib.paperless.spring.repository.ProgressRepository;

@Service("progressService")
public class ProgressService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProgressService.class);
	
	@Autowired
	ProgressRepository progressMapper;
	
	public ProgressStatus getConfirmDetail(HashMap<String, String> paramsMap){
		return progressMapper.selectProgressProgressStatusById(paramsMap);
	}
	
	public Progress selectProgressById(String progressId){
		return progressMapper.selectProgressById(progressId);
	}
	
	
	public List<Progress> selectProgressListByUserKey(HashMap<String, String> paramsMap){
		return progressMapper.selectProgressListByUserKey(paramsMap);
	}
	
	public List<ProgressStatus> selectListProgressProgressStatusByUserKey(HashMap<String, String> paramsMap){
		return progressMapper.selectListProgressProgressStatusByUserKey(paramsMap);
	}
	
	public List<ProgressStatus> selectListProgressProgressStatusByIdUserKey(HashMap<String, String> paramsMap){
		return progressMapper.selectListProgressProgressStatusByIdUserKey(paramsMap);
	}
	
	public Progress selectOneProgressListByUserKey(HashMap<String, String> paramsMap){
		return progressMapper.selectOneProgressListByUserKey(paramsMap);
	}
	
	public List<EformAttach> selectProgressEfromAttachList(String progressId){
		return progressMapper.selectProgressEformAttachList(progressId);
	}
	
	public EformUserData selectEformUserData(String progressId) {
		return progressMapper.selectEformUserDataById(progressId);
	}
	
	public EformUserData eformUserdataSelectOne(String progressId) {
		return progressMapper.eformUserdataSelectOne(progressId);
	}
	
	public int eformUserDataInsert(EformUserData entity){
		return progressMapper.eformUserDataInsert(entity);
	}
	
	public int progressStatusUpdate(Map map){
		return progressMapper.progressStatusUpdate(map);
	}
	
	public int identityAuthInfoInsert(IdentityAuthInfo identityAuthInfo){
		return progressMapper.identityAuthInfoInsert(identityAuthInfo);
	}
	
	public int identityAuthInfoDelete(IdentityAuthInfo identityAuthInfo){
		return progressMapper.identityAuthInfoDelete(identityAuthInfo);
	}
	public IdentityAuthInfo indentityAuthInfoSelectOne(IdentityAuthInfo identityAuthInfo){
		return progressMapper.indentityAuthInfoSelectOne(identityAuthInfo);
	}
}