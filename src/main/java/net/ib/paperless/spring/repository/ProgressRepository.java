package net.ib.paperless.spring.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import net.ib.paperless.spring.domain.EformAttach;
import net.ib.paperless.spring.domain.EformUserData;
import net.ib.paperless.spring.domain.Progress;
import net.ib.paperless.spring.domain.ProgressStatus;
import net.ib.paperless.spring.domain.IdentityAuthInfo;

@Repository
public class ProgressRepository {
	@Autowired
	private SqlSession sqlSession;

	public int progressInsert(Progress progress){
		return sqlSession.insert("progressMapper.progressInsert",progress);
	}
	
	public List<Progress> selectProgressByUserKey(String userKey){
		return sqlSession.selectList("progressMapper.selectProgressByUserKey",userKey);
	}
	
	public int progressStatusInsert(ProgressStatus progressStatus){
		return sqlSession.insert("progressMapper.insertProgressStatus",progressStatus);
	}
	
	public List<ProgressStatus> selectListProgressProgressStatusByUserKey(HashMap<String, String> paramsMap){
		return sqlSession.selectList("progressMapper.selectListProgressProgressStatusByUserKey",paramsMap);
	}
	
	public List<ProgressStatus> selectListProgressProgressStatusByIdUserKey(HashMap<String, String> paramsMap){
		return sqlSession.selectList("progressMapper.selectListProgressProgressStatusByIdUserKey",paramsMap);
	}
	
	public ProgressStatus selectProgressProgressStatusById(HashMap<String, String> paramsMap){
		return sqlSession.selectOne("progressMapper.selectProgressProgressStatusById",paramsMap);
	}
	
	public Progress selectProgressById(String progressId) {
		return sqlSession.selectOne("progressMapper.selectProgressById", progressId);
	}

	public List<Progress> selectProgressListByUserKey(HashMap<String, String> paramsMap) {
		return sqlSession.selectList("progressMapper.selectProgressListByUserKey", paramsMap);
	}
	
	public Progress selectOneProgressListByUserKey(HashMap<String, String> paramsMap) {
		return sqlSession.selectOne("progressMapper.selectOneProgressListByUserKey", paramsMap);
	}

	public List<EformAttach> selectProgressEformAttachList(String progressId) {
		return sqlSession.selectList("progressMapper.selectProgressEformAttachList", progressId);
	}
	
	public EformUserData selectEformUserDataById(String progressId) {
		return sqlSession.selectOne("progressMapper.selectEformUserDataById", progressId);
	}
	
	public EformUserData eformUserdataSelectOne(String progressId){
		return sqlSession.selectOne("progressMapper.eformUserdataSelectOne", progressId);
	}
	
	public int eformUserDataInsert(EformUserData entity){
		return sqlSession.insert("progressMapper.eformUserDataInsert", entity);
	}
	
	public int progressStatusUpdate(Map map){
		return sqlSession.update("progressMapper.progressStatusUpdate", map);
	}
	
	public int identityAuthInfoInsert(IdentityAuthInfo identityAuthInfo){
		return sqlSession.insert("progressMapper.identityAuthInfoInsert", identityAuthInfo);
	}
	
	public int identityAuthInfoDelete(IdentityAuthInfo identityAuthInfo){
		return sqlSession.delete("progressMapper.identityAuthInfoDelete",identityAuthInfo);
	}
	
	public IdentityAuthInfo indentityAuthInfoSelectOne(IdentityAuthInfo identityAuthInfo){
		return sqlSession.selectOne("progressMapper.indentityAuthInfoSelectOne",identityAuthInfo);
	}
}