package net.ib.paperless.spring.repository;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import net.ib.paperless.spring.domain.LoanInfo;
import net.ib.paperless.spring.domain.SmsAuthCodeInfo;

@Repository
public class InquiryRepository {
	@Autowired
	private SqlSession sqlSession;
	
	public LoanInfo loanInfoSelect(String loanId){
		return sqlSession.selectOne("inquiryMapper.loanInfoSelect",loanId);
	}
	
	public int authCodeInsert(SmsAuthCodeInfo codeInfo){
		Integer insertSmsCode = sqlSession.insert("inquiryMapper.authCodeInsert",codeInfo);
		if(insertSmsCode == 1)
			return 1;
		
		return 0;
	}
	
	
	public int smsAuthCodeInsert(SmsAuthCodeInfo codeInfo){
		Integer insertSmsCode = sqlSession.insert("inquiryMapper.smsAuthCodeInsert",codeInfo);
		if(insertSmsCode == 1)
		{
			return sqlSession.update("inquiryMapper.sendSmsStatusUpdate",codeInfo);
		}
		
		return 0;
	}
	
	public int sendSmsStatusReset(String progress_id){
		return sqlSession.update("inquiryMapper.sendSmsStatusReset",progress_id);
	}
	
	public int compareAuthCodeStatusUpdate(String progress_id)
	{
		Integer statusUpdate = sqlSession.update("inquiryMapper.compareAuthCodeStatusUpdate",progress_id);
		
		if(statusUpdate == 1)
		{
			return sqlSession.delete("inquiryMapper.deleteAuthCodeDelete",progress_id);
		}
		return 0;
	}
	
	public SmsAuthCodeInfo smsAuthCodeSelect(String progress_id){
		return sqlSession.selectOne("inquiryMapper.smsAuthCodeSelect",progress_id);
	}
	
	public int smsAuthCodeUpdate(String progress_id){
		return sqlSession.update("inquiryMapper.smsAuthCodeUpdate",progress_id);
	}
	
	public int authCodeUpdate(SmsAuthCodeInfo codeInfo){
		return sqlSession.update("inquiryMapper.authCodeUpdate",codeInfo);
	}

	public int smsAuthCodeDelete(String progress_id){
		return sqlSession.delete("inquiryMapper.deleteAuthCodeDelete",progress_id);
	}
	
	public SmsAuthCodeInfo authCodeSelect(SmsAuthCodeInfo codeInfo){
		return sqlSession.selectOne("inquiryMapper.authCodeSelect",codeInfo);
	}
	
	public int authCodeDelete(SmsAuthCodeInfo codeInfo){
		return sqlSession.delete("inquiryMapper.authCodeDelete",codeInfo);
	}
	
	public int appInfoCheckSelectOne(Map map){
		return sqlSession.selectOne("userMapper.appInfoCheckSelectOne",map);
	}
}