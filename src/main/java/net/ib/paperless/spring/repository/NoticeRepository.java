package net.ib.paperless.spring.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import net.ib.paperless.spring.domain.NoticeInfo;

@Repository
public class NoticeRepository {
	@Autowired
	private SqlSession sqlSession;
	
	public List<NoticeInfo> NoticeListSelect(String loanId, String page){
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("loanId",loanId);
		paramMap.put("page", page);
		
		return sqlSession.selectList("noticeMapper.GetNotice",paramMap);
	}
	
}