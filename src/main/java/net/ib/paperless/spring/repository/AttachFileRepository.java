package net.ib.paperless.spring.repository;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.ib.paperless.spring.domain.EformAttach;
import net.ib.paperless.spring.domain.ProgressAttach;

@Repository
public class AttachFileRepository implements Serializable {
	@Autowired
	private SqlSession sqlSession;
	
	private String progress_id;
	private String attach_id;
	
	@JsonIgnore
	private MultipartFile attach_file;
	
    @JsonProperty("attach_file")
    private String fileName;

    
	public String getProgress_id() {
		return progress_id;
	}

	public void setProgress_id(String progress_id) {
		this.progress_id = progress_id;
	}

	public String getAttach_id() {
		return attach_id;
	}

	public void setAttach_id(String attach_id) {
		this.attach_id = attach_id;
	}

	public MultipartFile getAttach_file() {
		return attach_file;
	}

	public void setAttach_file(MultipartFile attach_file) {
		this.attach_file = attach_file;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
 
	public List<EformAttach> eFormAttachSelectByProgressId(String progressId) {
		return sqlSession.selectList("AttachMapper.EFormAttachSelectByProgressId", progressId);
	}

	public EformAttach eFormAttachSelectByAttachId(HashMap<String, Object> paramsMap) {
		return sqlSession.selectOne("AttachMapper.EFormAttachSelectByAttachId", paramsMap);
	}
	
	public int progressAttachInsert(ProgressAttach progressAttach){
		return sqlSession.insert("AttachMapper.progressAttachInsert",progressAttach);
	}
}
