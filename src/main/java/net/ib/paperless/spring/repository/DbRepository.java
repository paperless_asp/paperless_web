package net.ib.paperless.spring.repository;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DbRepository {

	@Autowired
	//@Qualifier("sqlSessionTemplate")
	private SqlSession sqlSession;

	/* DB Select */
	public String getDual() {
		return sqlSession.selectOne("dbMapper.getDual");
	}
}