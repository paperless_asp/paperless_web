package net.ib.paperless.spring.domain;

import lombok.Data;

//@Data
public class ProgressAttach extends ProgressStatus{
	private int seq;
	private String progress_id;
	private String eform_attach_id;
	private String path;
	private int upload_yn;
	private String upload_date;
	private int transfer_yn;
	private String transfer_date;
	private int admin_confirm_yn;
	private String admin_confirm_date;
	private String note;
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public String getProgress_id() {
		return progress_id;
	}
	public void setProgress_id(String progress_id) {
		this.progress_id = progress_id;
	}
	public String getEform_attach_id() {
		return eform_attach_id;
	}
	public void setEform_attach_id(String eform_attach_id) {
		this.eform_attach_id = eform_attach_id;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public int getUpload_yn() {
		return upload_yn;
	}
	public void setUpload_yn(int upload_yn) {
		this.upload_yn = upload_yn;
	}
	public String getUpload_date() {
		return upload_date;
	}
	public void setUpload_date(String upload_date) {
		this.upload_date = upload_date;
	}
	public int getTransfer_yn() {
		return transfer_yn;
	}
	public void setTransfer_yn(int transfer_yn) {
		this.transfer_yn = transfer_yn;
	}
	public String getTransfer_date() {
		return transfer_date;
	}
	public void setTransfer_date(String transfer_date) {
		this.transfer_date = transfer_date;
	}
	public int getAdmin_confirm_yn() {
		return admin_confirm_yn;
	}
	public void setAdmin_confirm_yn(int admin_confirm_yn) {
		this.admin_confirm_yn = admin_confirm_yn;
	}
	public String getAdmin_confirm_date() {
		return admin_confirm_date;
	}
	public void setAdmin_confirm_date(String admin_confirm_date) {
		this.admin_confirm_date = admin_confirm_date;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
}