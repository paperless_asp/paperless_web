package net.ib.paperless.spring.domain;

import java.util.Date;

public class NoticeInfo {
	private Integer seq;
	private String loan_id;
	private String admin_id;
	private String title;
	private String body;
	private Date reg_date;
	private Date modify_date;
	private Boolean view_yn;
	

	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getLoan_id() {
		return loan_id;
	}
	public void setLoan_id(String loan_id) {
		this.loan_id = loan_id;
	}
	public String getAdmin_id() {
		return admin_id;
	}
	public void setAdmin_id(String admin_id) {
		this.admin_id = admin_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Date getReg_date() {
		return reg_date;
	}
	public void setReg_date(Date reg_date) {
		this.reg_date = reg_date;
	}
	public Date getModify_date() {
		return modify_date;
	}
	public void setModify_date(Date modify_date) {
		this.modify_date = modify_date;
	}
	public Boolean getView_yn() {
		return view_yn;
	}
	public void setView_yn(Boolean view_yn) {
		this.view_yn = view_yn;
	}
	
}
