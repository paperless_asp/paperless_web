package net.ib.paperless.spring.support;

import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.springframework.stereotype.Component;

@Component
public class SessionManager implements HttpSessionBindingListener {

	private static SessionManager sessionManager = null;
	private static Hashtable<String, HttpSession> loginUsers = new Hashtable<String, HttpSession>(); 
	
	private SessionManager(){
		super();
	}
	
	public static synchronized SessionManager getInstance() {
		if (sessionManager == null) {
			sessionManager = new SessionManager();
		}
		return sessionManager;
	}
	
	public boolean isValid(String userId, String remoteAddr) {
		return true;
	}
	
	/**
	 * 기존 로그인 여부 확인
	 * true 이면 
	 * @param sessionId
	 * @param remoteAddr
	 * @return boolean
	 */
	public boolean chkValidLogin(String req_session_id, String remoteAddr) {
		boolean isLogin = true;             
		
		Enumeration<String> e = loginUsers.keys();  
		String session_id = "";
		boolean isExist = false;
		while(e.hasMoreElements()) {                                   
		    session_id = (String)e.nextElement();		    
//		    HttpSession session = (HttpSession)loginUsers.get(key);
		    HttpSession session = getHttpSession(session_id);
		    try {
		    	System.out.println("MAP KEY : " + session_id + " SESSIONID : " + req_session_id);
		    	System.out.println("MAP ADDR : " + (String)session.getAttribute("remoteaddr") + " SESSIONADDR : " + remoteAddr);
		    	
		    	if (req_session_id.equals(session_id)) {
		    		isExist = true;
		    		if (!session.getAttribute("remoteaddr").toString().equals(remoteAddr)) {
		    			isLogin = false;
		    		}
		    	}
		    } catch (IllegalStateException ex) {
		    	System.out.println("First Login : " + req_session_id);
		    	loginUsers.remove(session_id);
		    	return true;
		    }
		}
		System.out.println("CHECK LOGIN : " + isLogin);
		return isLogin;
	}
	
	public HttpSession getHttpSession (String sessionId) {
		if (!loginUsers.containsKey(sessionId)) {
			return null;
		} else { 
			return (HttpSession)loginUsers.get(sessionId);
		}
	}
	
	public void setHttpSession (String sessionId, HttpSession session, String remoteAddr) {
		if (!chkValidLogin(sessionId, remoteAddr)) {
			removeSession(sessionId);
		}
		loginUsers.put(sessionId, session);
	}
	
	// 세션종료
	public void removeSession (String sessionId) {
		try {
			HttpSession session = (HttpSession) loginUsers.get(sessionId);
			if (session != null) {
				session.invalidate();
				loginUsers.remove(sessionId);
			}
		} catch (IllegalStateException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		// TODO Auto-generated method stub
		try {
			HttpSession session = loginUsers.get(event.getSession().getId());
			session.invalidate();
			loginUsers.remove(event.getSession().getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
