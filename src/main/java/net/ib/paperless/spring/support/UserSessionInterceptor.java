package net.ib.paperless.spring.support;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import net.ib.paperless.spring.domain.User;
import net.ib.paperless.spring.service.InquiryService;

@Component
public class UserSessionInterceptor extends HandlerInterceptorAdapter{
	@Autowired
	InquiryService inquiryService;
	
	@Autowired
	SessionManager sessionManager;
	
	@Override 
	public boolean preHandle( HttpServletRequest request, HttpServletResponse response, Object handler ) throws Exception { 

		System.out.println("--------------------------------");
		String remoteAddr = request.getHeader("REMOTE_ADDR");
		System.out.println(remoteAddr + "/" + request.getRemoteHost() + "/" + request.getRemoteUser() + "/" + request.getRequestedSessionId());
		System.out.println("--------------------------------");		
		
		// HTTP 요청 처리 전 수행할 로직 작성 
		//session 체크
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		String reqUri = request.getRequestURI().toString();
		System.out.println("user : " + user);
		System.out.println("reqUri : " + reqUri);
		System.out.println("reqUri : " + reqUri.indexOf("/api/"));
		Map<?, ?> pathVariables = (Map<?, ?>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
		//if(user == null && !reqUri.equals("/null/main")){
		if(pathVariables.get("loanId") != null){
			System.out.println("loanInfoSelect : " + inquiryService.loanInfoSelect(pathVariables.get("loanId").toString()));	
		}
		
	    String referrer = request.getHeader("Referer");
	    System.out.println("referrer : " + referrer);
		
		if(user == null){
			if(reqUri.indexOf("/api/") >=0){
				response.sendRedirect("/api/checkLogin");
			}else if(reqUri.indexOf("/error") >=0 && referrer != null) {
				response.sendRedirect("/error.jsp");
			}else{
				response.sendRedirect("/"+ pathVariables.get("loanId")+ "/main");	
			}	
			return false;
		}
		return true; 
	}
	
	@Override 
	public void postHandle( HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView )throws Exception { 
		// HTTP 요청 처리 후 수행할 로직 작성 }
		System.out.println("============postHandle");
	}

}