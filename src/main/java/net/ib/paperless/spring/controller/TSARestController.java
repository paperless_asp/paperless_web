package net.ib.paperless.spring.controller;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.ib.paperless.spring.common.UUIDs;
import net.ib.paperless.spring.domain.EformUserData;
import net.ib.paperless.spring.domain.Progress;
import net.ib.paperless.spring.domain.Response;
import net.ib.paperless.spring.domain.User;
import net.ib.paperless.spring.service.InquiryService;
import net.ib.paperless.spring.service.ProgressService;
import net.ib.paperless.spring.service.TsaService;



@RestController("tsaRestController")
public class TSARestController {
	
	private static final Logger logger = LoggerFactory.getLogger(TSARestController.class);
	
	@Autowired
	TsaService tsaService;
	
	@Autowired
	ProgressService progressService;
	
	@Autowired
	InquiryService inquiryService;
	
	@RequestMapping({"/tsa"})
	public Response tsa(@RequestParam(value="progress_id", required=true) String progress_id) {
		Response response = new Response();
		System.out.println(progress_id);
		
//		progressService.getConfirmDetail(paramsMap)
		
		boolean ret = tsaService.timestampCertificate(progress_id);
		if (ret) {
			response.setResponse("OK");
			response.setNote("완료");	
			
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("eform_yn", 1);
			map.put("progressId", progress_id);
			if(progressService.progressStatusUpdate(map)<=0){
				response.setResponse("FAIL");
				response.setNote("실패");
				logger.info("Status Update Fail [{}]", progress_id);
			}
		} else {			
			response.setResponse("FAIL");
			response.setNote("실패");
			logger.info("TSA Certifacate Fail [{}]", progress_id);
		}
		return response;
	}
	
	// 서식 노출을 위한 사용자 데이터 조회
	@RequestMapping({"/eformUserData"})
//	public EformUserData getEformUserData(@RequestParam(value="progress_id", required=true) String progress_id) {
	public HashMap<String, Object> getEformUserData(@RequestParam(value="progress_id", required=true) String progress_id , HttpServletRequest request) {
		HashMap<String, Object> ret = new HashMap<String, Object>();
		
		EformUserData eformUserData = progressService.selectEformUserData(progress_id);
		
		if (eformUserData == null) {
			return null;
		}
		
//		eformUserData.setExpire_date("2018-02-30 23:59");
//		eformUserData.setInterest_day("매달 25일");
//		eformUserData.setInterest_per("고정 15퍼");
//		eformUserData.setLoan_method("여신실행방법? 모름");
//		eformUserData.setReg_date("나우");
//		eformUserData.setRepayment_commission("없음");
//		eformUserData.setRepayment_method("만기일 전액상환");
//		eformUserData.setStart_date("2017-02-30 23:59");
//		
//		eformUserData.setAmount("3434");
//		eformUserData.setLoan_name("직장인 대출");
		
		
		
		Map<String,Object> jMap = new HashMap<String,Object>();
		String telNumber = null;
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("user");
		if(user.getUserPhone() != null){
			telNumber = user.getUserPhone();	
		}
		String uuids = UUIDs.createNameUUID(telNumber.getBytes()).toString();
		System.out.println("uuids : " + uuids);
		Progress progressInfoVo = progressService.selectProgressById(progress_id);
		System.out.println("progressListVo : " + progressInfoVo);
		if(progressInfoVo != null){
			jMap = inquiryService.getFtpJsonData(progressInfoVo.getLoan_id() , uuids , progressInfoVo.getSeq(),progressInfoVo.getReg_date());
		}
		System.out.println("jMap : " + jMap);
		String account = "";
		String bank_name ="";
		String userName = "";
		String jumin = "";
		if (jMap.get("name") != null) { userName = jMap.get("name").toString(); }
		if (jMap.get("account_holder_number") != null) { account = jMap.get("account_holder_number").toString(); }
		if (jMap.get("account_bank_name") != null) { bank_name = jMap.get("account_bank_name").toString(); }
		if (jMap.get("jumin") != null) { jumin = jMap.get("jumin").toString(); }
		eformUserData.setRegistration_number(jumin + "-*******");
		eformUserData.setAccount(bank_name + " " + account);
		eformUserData.setUser_name(userName);
		eformUserData.setInterest_per(eformUserData.getInterest_per() + " " + eformUserData.getInterest_per_memo());
		
		DecimalFormat df = new DecimalFormat("#,##0");
		int amount = Integer.parseInt(eformUserData.getAmount()) * 10000;	// 만원->원단위 변환 
		String str_amount = df.format(amount);
		eformUserData.setAmount(str_amount + " 원");
		
		ret.put("map", eformUserData);
		return ret;
	}
}
