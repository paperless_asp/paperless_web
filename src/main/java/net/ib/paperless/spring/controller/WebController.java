package net.ib.paperless.spring.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.clipsoft.org.apache.commons.io.IOUtils;

import net.ib.paperless.spring.common.DateUtil;
import net.ib.paperless.spring.common.UUIDs;
import net.ib.paperless.spring.domain.LoanInfo;
import net.ib.paperless.spring.domain.NoticeInfo;
import net.ib.paperless.spring.domain.SmsAuthCodeInfo;
import net.ib.paperless.spring.domain.User;
import net.ib.paperless.spring.openplatform.OpenPlatformAPI;
import net.ib.paperless.spring.repository.InquiryRepository;
import net.ib.paperless.spring.service.InquiryService;
import net.ib.paperless.spring.service.NoticeService;
import net.ib.paperless.spring.service.ProgressService;
import net.ib.paperless.spring.supersms.SupersmsAPI;
import net.ib.paperless.spring.support.SessionManager;

@Controller
public class WebController {
		
	@Autowired
	NoticeService noticeService;
	
	@Autowired
	InquiryService inquiryService;
	
	@Autowired
	ProgressService progressService;

	@Autowired
	InquiryRepository inquiryMapper;
	
	@Autowired
	SessionManager sessionManager;
	
	private String localPath = "C:\\temp\\";
	
	@RequestMapping(value = {"/robots", "/robot", "/robot.txt", "/robots.txt"}, method={RequestMethod.GET , RequestMethod.POST})
	public void robot(HttpServletResponse response) {
	 
	    InputStream resourceAsStream = null;
	    try {
	        ClassLoader classLoader = getClass().getClassLoader();
	        resourceAsStream = classLoader.getResourceAsStream("robots.txt");
	 
	        response.addHeader("Content-disposition", "filename=robots.txt");
	        response.setContentType("text/plain");
	        IOUtils.copy(resourceAsStream, response.getOutputStream());
	        response.flushBuffer();
	        System.out.println("ROBOT REQ");
	    } catch (Exception e) {
	        e.printStackTrace();
	    } finally {
	    	try {
				resourceAsStream.close();
			} catch (IOException e) {
				if (resourceAsStream != null) {
					resourceAsStream = null;
				}
				e.printStackTrace();
			}
	    }
	}
	
	@RequestMapping(value = {"/{loanId}", "/{loanId}/index", "/{loanId}/dashboard"}, method={RequestMethod.GET , RequestMethod.POST})
	public String index(Model model , @PathVariable String loanId) {
		Map<String,Object> map = new HashMap<String,Object>();
		System.out.println("loanId : " + loanId );
		LoanInfo entity = inquiryService.loanInfoSelect(loanId);
		
		map.put("loanName", entity.getName());
		map.put("loanId", loanId);
		map.put("loanInfo", entity);
		model.addAttribute("map", map);
		return "/main";
	}
	
	/**
	 * main page
	 * @param model
	 * @param loanId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value ={"/{loanId}/main"}, method={RequestMethod.GET , RequestMethod.POST})
	public String main(Model model , @PathVariable String loanId) throws Exception {
		Map<String,Object> map = new HashMap<String,Object>();
		LoanInfo entity = inquiryService.loanInfoSelect(loanId);
		
		if(!loanId.equals("null") && loanId != null && entity != null)
			map.put("loanName", entity.getName());
		
		if (entity == null && !loanId.equals("null")) {
			return "/error";
		}
		
//		System.out.println("aaaa : " + loanId);
		
		map.put("loanId", loanId);
		map.put("loanInfo", entity);
		
		model.addAttribute("map", map);
		//System.out.println("llll : " + loanId);
		//inquiryService.setLoanUserInfo("01095127232", loanId);
		return "/main";
	}
	

	
	@RequestMapping(value={"/inquiry/detail"}, method={RequestMethod.GET , RequestMethod.POST})
	public String detail(Model model , @PathVariable String loanId) {
		Map<String,Object> map = new HashMap<String,Object>();
		LoanInfo entity = inquiryService.loanInfoSelect(loanId);
		map.put("loanName", entity.getName());
		model.addAttribute("map", map);
		return "/inquiry/detail";
	}


	@RequestMapping(value ={"/{loanId}/inquiry/account_verify"}, method={RequestMethod.GET , RequestMethod.POST})
	public String accountVerify(Model model, @PathVariable String loanId) {

		HashMap<String, Object> bankList = (HashMap<String, Object>) OpenPlatformAPI.getBankStatus();
		Map<String,Object> map = new HashMap<String,Object>();
		LoanInfo entity = inquiryService.loanInfoSelect(loanId);
		map.put("loanName", entity.getName());
		map.put("loanId", loanId);	
		map.put("bankList", bankList.get("res_list"));
		model.addAttribute("map", map);
		
		return "/inquiry/account_verify";
	}
	
	@ResponseBody
	@RequestMapping(value ={"/{loanId}/inquiry/accountCheck"}, method={RequestMethod.GET , RequestMethod.POST})
	public Map<String, Object> accountCheck( HttpServletRequest request, HttpServletResponse response) throws Exception
    {
		Map<String,Object> map = new HashMap<String,Object>();
		
		String account_holder_number = request.getParameter("account_holder_number");
		String account_holder_code = request.getParameter("account_holder_code");
		String ssn_number = request.getParameter("ssn_number");
		String account_bank_name = request.getParameter("account_bank_name");
		String progress_id = request.getParameter("progress_id");
		String user_key = request.getParameter("user_key");
		String loan_id = request.getParameter("loan_id");
		String id = request.getParameter("id");
		
		
		
		HashMap<String, Object> realNameResult = (HashMap<String, Object>) OpenPlatformAPI.getRealName(account_holder_code, account_holder_number, ssn_number);
		
		if(realNameResult.get("rsp_code").equals("A0000"))
		{
			String account_holder_name = (String)realNameResult.get("account_holder_name");
			Random generator = new Random();   
//			String random_code = String.valueOf(1000+generator.nextInt(9000)); 
			String random_code = "1234";	//임시
			
			HashMap<String, Object> result = (HashMap<String, Object>) OpenPlatformAPI.setTransderDeposit(account_bank_name, account_holder_code, account_holder_number, account_bank_name, account_holder_name,  random_code);
			String rsp_code = (String)result.get("rsp_code");
			
			if(rsp_code.equals("A0000")) {
				
				SmsAuthCodeInfo codeInfo = new SmsAuthCodeInfo();
				codeInfo.setProgress_id(progress_id);
				codeInfo.setCode(random_code);
				codeInfo.setUser_key(user_key);
				
				inquiryService.setSmsAuthCode(codeInfo);
				
				HashMap<String, Object> accountInfo = new HashMap<String,Object>();
				accountInfo.put("account_holder_number", account_holder_number);
				accountInfo.put("account_bank_name", account_bank_name);
				accountInfo.put("account_yn", "N");
				
				inquiryService.setAccountAuthInfo(id, loan_id, user_key, accountInfo);
				
				map.put("rsp_code", "1");	
				map.put("rsp_msg", "요청하신 계좌의 실명인증이 완료되었습니다. \n입금자 명의에 기재된 4자리 인증코드를 확인하시고\n 계좌인증을 완료해 주세요.");
				
				return map;
				
			}
			
			map.put("rsp_code", "2");
			map.put("rsp_msg", "입금이체 중 오류가 발생하였습니다.\n잠시 후 다시 이용해 주시기 바랍니다.");
			return map;
		}
	
	
		map.put("rsp_code", "3");
		map.put("rsp_msg", "인증정보가 올바르지 않습니다.\n잠시 후 다시 이용해 주시기 바랍니다.");
		return map;
	}
	
	//휴대폰 인증번호 발송
	@ResponseBody
	@RequestMapping(value ={"/{loanId}/inquiry/sendAuthCode"}, method={RequestMethod.GET , RequestMethod.POST})
	public Map<String, Object> sendAuthCode( HttpServletRequest request, HttpServletResponse response) throws Exception
    {
		String tel_number = request.getParameter("tel_number");
		String loan_id = request.getParameter("loan_id");
		
		Random generator = new Random();   
		String random_code = String.valueOf(1000+generator.nextInt(9000)); 
		String text = "GoodPaper 인증번호는 [" + random_code + "]입니다.";
		SupersmsAPI.sendMessage("0316281586", text, tel_number);

//		String random_code = "1234";
		
		System.out.println("tel_number = " + tel_number);
		System.out.println("loan_id = " + loan_id);
		System.out.println("random_code = " + random_code);
		
		SmsAuthCodeInfo codeInfo = new SmsAuthCodeInfo();
		codeInfo.setTel_number(tel_number);
		codeInfo.setLoan_id(loan_id);
		codeInfo.setCode(random_code);
		return inquiryService.setAuthCode(codeInfo);
		
	}
	
	//인증코드 확인
	@ResponseBody
	@RequestMapping(value ={"/{loanId}/inquiry/authCodeConfirm"}, method={RequestMethod.GET , RequestMethod.POST})
	public int authCodeConfirm( HttpServletRequest request, HttpServletResponse response) throws Exception
    {

		String input_smsAuthCode = request.getParameter("code");
		String tel_number = request.getParameter("tel_number");
		LoanInfo loanInfo = inquiryService.loanInfoSelect(request.getParameter("loan_id").toString());
		SmsAuthCodeInfo codeInfo = new SmsAuthCodeInfo();
		codeInfo.setTel_number(tel_number);
		codeInfo.setLoan_id(request.getParameter("loan_id").toString());
		
		SmsAuthCodeInfo resutl_codeInfo = inquiryService.authCodeSelect(codeInfo);
		System.out.print("input authcode = "+input_smsAuthCode);
		System.out.print("authcode = "+ resutl_codeInfo.getCode());
		System.out.print("fail count = "+ resutl_codeInfo.getCount());
		
		if(resutl_codeInfo.getCode().equals(input_smsAuthCode) && resutl_codeInfo.getCount() != 4)
		{
			int delestatus = inquiryService.authCodeDelete(codeInfo);
			
			if(delestatus == 1){
				
				HttpSession session = request.getSession();
				User user = new User();
				user.setLoanId(request.getParameter("loan_id").toString());
				user.setLoanName(loanInfo.getName());
				user.setUserPhone(tel_number);
				String uuids = UUIDs.createNameUUID(tel_number.getBytes()).toString();
				user.setUserKey(uuids);
				session.setAttribute("user", user);
				
//				String sessionId = request.changeSessionId();
				String sessionId = request.getRequestedSessionId();
				System.out.println("SESSION-ID : " + sessionId);
				String remoteAddr = request.getHeader("REMOTE_ADDR");
				session.setAttribute("remoteaddr", remoteAddr);
				sessionManager.setHttpSession(sessionId, session, remoteAddr);
				
				return 1;
			}
			return delestatus;
		}
		else if(resutl_codeInfo.getCount() == 4){
			inquiryService.authCodeDelete(codeInfo);
			return 2;
			
		}
		
		inquiryService.authCodeUpdate(codeInfo);
		return 0;
		
	}
	
	/*
	//인증코드 발송
	@ResponseBody
	@RequestMapping(value ={"/{loanId}/inquiry/transderDeposit"}, method={RequestMethod.GET , RequestMethod.POST})
	public Map<String, Object> transderDeposit( HttpServletRequest request, HttpServletResponse response) throws Exception
    {

		String account_holder_number = request.getParameter("account_holder_number");
		String account_holder_code = request.getParameter("account_holder_code");
		String account_bank_name = request.getParameter("account_bank_name");
		String account_holder_name = request.getParameter("account_holder_name");
		String progress_id = request.getParameter("progress_id");
		String user_key = request.getParameter("user_key");
		String loan_id = request.getParameter("loan_id");
		String id = request.getParameter("id");
		
		Random generator = new Random();   
//		String random_code = String.valueOf(1000+generator.nextInt(9000)); 
		String random_code = "1234";	//임시
		
		HashMap<String, Object> result = (HashMap<String, Object>) OpenPlatformAPI.setTransderDeposit(account_bank_name, account_holder_code, account_holder_number, account_bank_name, account_holder_name,  random_code);
		String rsp_code = (String)result.get("rsp_code");
		if(!rsp_code.equals("A0000")) {
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("rsp_code", (String)result.get("rsp_code"));
			return map;
		}
		
		//TODO 
		//성공 여부에 따른 분기처리가 되어 있어야함... 하지만 개발서버는 오픈API 조회가 정상적이지 않으니 우선 사용
		//1. 오픈 API 성공시에만 실행되도록 하고 2.insert 성공 실패에 따른 결과값을 return해주도록...
		SmsAuthCodeInfo codeInfo = new SmsAuthCodeInfo();
		codeInfo.setProgress_id(progress_id);
		codeInfo.setCode(random_code);
		codeInfo.setUser_key(user_key);
		
		inquiryService.setSmsAuthCode(codeInfo);
		
		//TODO
		//인증 성공시에만... 처리되도록
		HashMap<String, Object> accountInfo = new HashMap<String,Object>();
		accountInfo.put("account_holder_number", account_holder_number);
		accountInfo.put("account_bank_name", account_bank_name);
		accountInfo.put("account_yn", "N");
		
		inquiryService.setAccountAuthInfo(id, loan_id, user_key, accountInfo);
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("rsp_code", (String)result.get("rsp_code"));	
		
		return map;
	}
	*/
	//인증코드 확인
	@ResponseBody
	@RequestMapping(value ={"/{loanId}/inquiry/smsAuthCodeConfirm"}, method={RequestMethod.GET , RequestMethod.POST})
	public int smsAuthCodeConfirm( HttpServletRequest request, HttpServletResponse response) throws Exception
    {

		String progress_id = request.getParameter("progress_id");
		String input_smsAuthCode = request.getParameter("smsAuthCode");
		String loan_id = request.getParameter("loan_id");
		String user_key = request.getParameter("user_key");
		String id = request.getParameter("id");
		
		SmsAuthCodeInfo smsAuthCodeInfo = inquiryService.smsAuthCodeSelect(progress_id);
		System.out.println("smsAuthCodeInfo.getCount() =" + smsAuthCodeInfo.getCount());
		
		if(smsAuthCodeInfo.getCode().equals(input_smsAuthCode) && smsAuthCodeInfo.getCount() != 4)
		{
			int updateStatus = inquiryService.compareAuthCodeStatusUpdate(progress_id);
			
			if(updateStatus == 1){

				String currentDate = DateUtil.getCurrentDateString();
				
				HashMap<String, Object> accountInfo = new HashMap<String,Object>();
				accountInfo.put("account_regdate", currentDate);
				accountInfo.put("account_yn", "Y");
				
				HashMap<String, Object> result = new HashMap<String,Object>();
				result = (HashMap<String, Object>) inquiryService.setAccountAuthInfo(id, loan_id, user_key, accountInfo);
				if(result.get("result").equals(true)) {
					return 1;
				}
				return 0;
			}
			return updateStatus;
		}
		else if(smsAuthCodeInfo.getCount() == 4){
			
			inquiryService.sendSmsStatusReset(progress_id);
			inquiryService.smsAuthCodeDelete(progress_id);
			return 2;
		}
		
		inquiryService.smsAuthCodeUpdate(progress_id);
		return 0;
		
	}

	
	// 공지사항
	@RequestMapping(value ={"/{loanId}/info/notice"}, method={RequestMethod.GET , RequestMethod.POST})
	public String notice(Model model, @PathVariable String loanId) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("loanId", loanId);		
		model.addAttribute("map", map);
		return "/info/notice";
	}
	
	// 고객문의
	@RequestMapping(value ={"/{loanId}/info/inquire"}, method={RequestMethod.GET , RequestMethod.POST})
	public String inquire(Model model, @PathVariable String loanId) {

		LoanInfo loanInfo = inquiryService.loanInfoSelect(loanId);
		
		String telNumner = loanInfo.getTel_number().substring(0,4) + "-" + loanInfo.getTel_number().substring(4,8);
		String callCenter = loanInfo.getCall_center().substring(0,4) + "-" + loanInfo.getCall_center().substring(4,8);
		String name = loanInfo.getName();
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("name", name);		
		map.put("loanName", loanInfo.getName());
		map.put("loanId", loanId);		
		map.put("telNumber", telNumner);	
		map.put("callCenter", callCenter);	
		model.addAttribute("map", map);
		return "/info/inquire";
	}
	
	//공지사항 내역 가져오기
	@ResponseBody
	@RequestMapping(value ={"/notice/getNoticeList"}, method={RequestMethod.GET , RequestMethod.POST})
	public List<NoticeInfo> getNoticeList( HttpServletRequest request, HttpServletResponse response) throws Exception
    {
		
		String loanId = request.getParameter("loanId");
		String page = request.getParameter("page");
		
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("loanId", loanId);
		params.put("page", page);
		
		List<NoticeInfo> resultMap = noticeService.getNoticeList(params);

		return resultMap;
	}
	
	@RequestMapping(value ="/login", method={RequestMethod.GET , RequestMethod.POST})
	public String login() {
		return "login";
	}
}
