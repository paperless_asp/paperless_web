package net.ib.paperless.spring.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import net.ib.paperless.spring.common.ApiResponse;
import net.ib.paperless.spring.domain.EformAttach;
import net.ib.paperless.spring.service.AttachFileService;

@RestController
@RequestMapping("/attach/**")
public class AttachFileController {
	
	private static final Logger logger = LoggerFactory.getLogger(AttachFileController.class);
	
	@Autowired
	AttachFileService attachFileService;
	
	// 첨부 업로드
	@RequestMapping(value="/upload", method=RequestMethod.POST)
	public ApiResponse<Map<String,Object>> attachFile(@RequestParam("progress_id") String progress_id, 
			@RequestParam("attach_id") int attach_id,
			@RequestParam("attach") MultipartFile attach) {
		//private String localPath = "C:\\temp\\";
		//HttpSession session = request.getSession();
		//String root = session.getServletContext().getRealPath("/");
		
		logger.info("progress_id [{}]", progress_id);
		logger.info("attach_id [{}]", attach_id);
		logger.info("file name [{}]", attach.getOriginalFilename());
		System.out.println("attach : "+ attach);
		boolean result = attachFileService.eFormAttachFileSave(progress_id, attach_id, attach);
		System.out.println("result : "+ result);
		
/*		Response response = new Response();
		response.setResponse("OK");
		response.setNote("성공");
		return response;*/
		
		ApiResponse<Map<String , Object>> responseJson = new ApiResponse<Map<String,Object>>();
		responseJson.setResult(result);
		responseJson.setMessage("");
		return responseJson;
	}
	
	// 필수 첨부 리스트 조회
	@RequestMapping(value="/getAttachList", method={RequestMethod.GET , RequestMethod.POST})
	public ApiResponse<Map<String,Object>> getRequiredAttachList(@RequestParam("progress_id") String progress_id) {
		List<EformAttach> eFormList = attachFileService.eFormAttachSelectByProgressId(progress_id);

		ApiResponse<Map<String , Object>> responseJson = new ApiResponse<Map<String,Object>>();
		responseJson.setResult(true);
		responseJson.setTotalItems(eFormList.size());
		responseJson.setList(eFormList);
		return responseJson;
	}
}
