package net.ib.paperless.spring.controller;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;

public class ExceptionController implements ErrorController {
	
	private static final String PATH = "/error/error";
	
	@Override
	public String getErrorPath() {
		// TODO Auto-generated method stub
		return PATH;
	}

    @RequestMapping(value = "/error")
    public String error() {
        return "Error handling";
    }	
	
}
