package net.ib.paperless.spring.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import net.ib.paperless.spring.common.UUIDs;
import net.ib.paperless.spring.domain.EformAttach;
import net.ib.paperless.spring.domain.EformUserData;
import net.ib.paperless.spring.domain.LoanInfo;
import net.ib.paperless.spring.domain.Progress;
import net.ib.paperless.spring.domain.ProgressStatus;
import net.ib.paperless.spring.domain.User;
import net.ib.paperless.spring.openplatform.OpenPlatformAPI;
import net.ib.paperless.spring.service.InquiryService;
import net.ib.paperless.spring.service.ProgressService;
import net.ib.paperless.spring.support.SessionManager;

@Controller
@RequestMapping("/{loanId}/*")
public class ConfirmController{
	private String userKey = "2b97cb33-05e4-385b-a2ba-904cecff5601";
	@Autowired
	ProgressService progressService;
	
	@Autowired
	InquiryService inquiryService;
	
	@Autowired
	SessionManager sessionManager;
	/**
	 * info page
	 * @param model
	 * @param loanId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/confirm/info"})
	public String confirmInfo(Model model , @PathVariable String loanId , HttpServletRequest request, HttpServletResponse response) throws Exception {
		//13fc1050-3891-3225-b942-035d20827573
		System.out.println("loanId : " + loanId);
		HttpSession session = request.getSession();
		System.out.println("========================1" + session.getAttribute("user"));
		if(session.getAttribute("user") != null) {
			response.sendRedirect("/"+ loanId + "/confirm/list");
			return null;
		}else{
			Map<String,Object> map = new HashMap<String,Object>();
			LoanInfo entity = inquiryService.loanInfoSelect(loanId);
			map.put("loanName", entity.getName());
			map.put("loanId", loanId);
			model.addAttribute("map", map);
			return "/confirm/info";
		}
	}

	/**
	 * info page
	 * @param model
	 * @param loanId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/confirm/detail01",method={RequestMethod.GET , RequestMethod.POST})
	public String firstDetail(Model model , @PathVariable String loanId , HttpServletRequest request ,@RequestParam HashMap<String, String> paramsMap ) throws Exception {
		Map<String,Object> map = new HashMap<String,Object>();
		List<EformAttach> efEntityList = new ArrayList<EformAttach>();
		map.put("loanId", loanId);
		if(paramsMap.get("id") == null){
			System.out.println("파라미터 부족");
		}
		String remoteAddr = request.getHeader("REMOTE_ADDR");
		if (!sessionManager.chkValidLogin(request.getSession().getId(), remoteAddr)) {
//			String sessionId = request.changeSessionId();
			request.changeSessionId();
			return "/" + loanId +"/main";
		}
		paramsMap.put("user_key" , userKey);
		System.out.println("paramsMap : "+ paramsMap);
		ProgressStatus psEntity = progressService.getConfirmDetail(paramsMap);
		Progress pEntity = progressService.selectProgressById(paramsMap.get("id"));
		EformUserData efdEntity = progressService.selectEformUserData(paramsMap.get("id"));

		System.out.println("psEntity : "+ psEntity);
		System.out.println("pEntity : "+ pEntity);
		System.out.println("efdEntity : "+ efdEntity);
		map.put("psEntity", psEntity);
		map.put("pEntity", pEntity);
		map.put("efdEntity", efdEntity);
		
		if(psEntity != null){
			efEntityList = progressService.selectProgressEfromAttachList(paramsMap.get("id"));
		}
		map.put("efEntityList", efEntityList);
		
		// 계좌인증용 BankList - OpenPlatform
		HashMap<String, Object> bankList = (HashMap<String, Object>) OpenPlatformAPI.getBankStatus();
		map.put("bankList", bankList.get("res_list"));

		LoanInfo entity = inquiryService.loanInfoSelect(loanId);
		map.put("loanName", entity.getName());
		
		
		
		Map<String,Object> jMap = new HashMap<String,Object>();
		String telNumber = null;
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("user");
		if(user != null && user.getUserPhone() != null){
			telNumber = user.getUserPhone();	
		}else{
			telNumber = paramsMap.get("tel_number");
		}
		String uuids = UUIDs.createNameUUID(telNumber.getBytes()).toString();
		System.out.println("uuids : " + uuids);
		HashMap<String, String> pMap = new HashMap<String, String>();
		pMap.put("user_key", uuids);
		pMap.put("loanId", loanId);
		List<Progress> progressListVo = progressService.selectProgressListByUserKey(pMap);
		System.out.println("progressListVo : " + progressListVo);
		if(!progressListVo.isEmpty()){
			jMap = inquiryService.getFtpJsonData(loanId , uuids , progressListVo.get(0).getSeq(),progressListVo.get(0).getReg_date());
		}
		map.put("name", jMap.get("name"));
		map.put("id", paramsMap.get("id"));
		
		
		
		model.addAttribute("map", map);
		return "/confirm/detail01";
	}

	/**
	 * info page
	 * @param model
	 * @param loanId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/confirm/detail02"})
	public String secondDetail(Model model , @PathVariable String loanId) throws Exception {
		Map<String,Object> map = new HashMap<String,Object>();
		LoanInfo entity = inquiryService.loanInfoSelect(loanId);
		map.put("loanName", entity.getName());
		map.put("loanId", loanId);
		model.addAttribute("map", map);
		return "/confirm/detail02";
	}

	/**
	 * info page
	 * @param model
	 * @param loanId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/confirm/list",method={RequestMethod.GET , RequestMethod.POST})
	public String list(Model model , @PathVariable String loanId ,@RequestParam HashMap<String, String> paramsMap , HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String,Object> map = new HashMap<String,Object>();
		Map<String,Object> jMap = new HashMap<String,Object>();
		String telNumber = null;
		
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("user");
		
		if(user == null) {
			response.sendRedirect("/"+ loanId + "/main");
			return null;
		}
		String remoteAddr = request.getHeader("REMOTE_ADDR");
		if (!sessionManager.chkValidLogin(request.getSession().getId(), remoteAddr)) {
//			request.changeSessionId();
			System.out.println("Invalid Login IP : " + remoteAddr);
			response.sendRedirect("/"+ loanId + "/main");
			return null;
		}
		
		
		if(user.getUserPhone() != null){
			telNumber = user.getUserPhone();	
		}else{
			telNumber = paramsMap.get("tel_number");
		}
		String uuids = UUIDs.createNameUUID(telNumber.getBytes()).toString();
		System.out.println("uuids : " + uuids);
		HashMap<String, String> pMap = new HashMap<String, String>();
		pMap.put("user_key", uuids);
		pMap.put("loanId", loanId);
//		List<Progress> progressListVo = progressService.selectProgressListByUserKey(pMap);
		List<ProgressStatus> progressListVo = progressService.selectListProgressProgressStatusByIdUserKey(pMap);
		
		List<ProgressStatus> resetProgressListVo = new ArrayList<ProgressStatus>();		
		
		System.out.println("progressListVo : " + progressListVo);
		if(!progressListVo.isEmpty()){
			HashMap<String, Integer> dateMap = new HashMap<String, Integer>();
			// 날짜 중복에 Number 부여
			for (ProgressStatus progress : progressListVo) {
				ProgressStatus prog = new ProgressStatus();
				
				String status_text = getProgressStatusText(progress);
				
				prog = progress;
				prog.setStatus_name(status_text);
				String tmpDate = prog.getReg_date().substring(0, 10);
				//tmpDate = tmpDate.replaceAll("-", ".");
				//System.out.println(progress.getId() + ":" + dateMap.get(tmpDate) + ":" + tmpDate);
				if (dateMap.get(tmpDate) != null) {
					if (dateMap.containsKey(tmpDate)) {
						ProgressStatus lastPrg = resetProgressListVo.get(resetProgressListVo.size()-1);
						if (lastPrg.getReg_date().length() < 11) {
							lastPrg.setReg_date(lastPrg.getReg_date()+ " (1)");
							resetProgressListVo.remove(resetProgressListVo.size()-1);
							resetProgressListVo.add(lastPrg);
						}
						Integer cnt = dateMap.get(tmpDate);
						dateMap.remove(tmpDate);
						dateMap.put(tmpDate, ++cnt);
						prog.setReg_date(tmpDate + " (" + dateMap.get(tmpDate) + ")");
					} else {
						dateMap.put(tmpDate, 1);
						prog.setReg_date(tmpDate);
					}
				} else {
					dateMap.put(tmpDate, 1);
					prog.setReg_date(tmpDate);
				}
				resetProgressListVo.add(prog);
			}
			jMap = inquiryService.getFtpJsonData(loanId , uuids , progressListVo.get(progressListVo.size()-1).getSeq(),progressListVo.get(progressListVo.size()-1).getReg_date());
		}
		System.out.println("jMap : " + jMap);

		LoanInfo entity = inquiryService.loanInfoSelect(loanId);
		map.put("loanName", entity.getName());
		map.put("loanId", loanId);
		map.put("progressList", resetProgressListVo);
		map.put("jMap", jMap);
		System.out.println("progressList : " + resetProgressListVo);
		model.addAttribute("map", map);
		return "/confirm/list";
	}
	
	/**
	 * terms_agree page
	 * @param model
	 * @param loanId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/confirm/terms_agree",method={RequestMethod.GET , RequestMethod.POST})
	public String terms_agree(Model model , @PathVariable String loanId,@RequestParam HashMap<String, String> paramsMap , HttpServletRequest request) throws Exception {
		Map<String,Object> map = new HashMap<String,Object>();
		String progressId = paramsMap.get("progress_id");
		LoanInfo entity = inquiryService.loanInfoSelect(loanId);
		map.put("loanName", entity.getName());
		map.put("loanId", loanId);
		map.put("progressId", progressId);
		model.addAttribute("map", map);
		return "/confirm/terms_agree";
	}
	
	
	private String getProgressStatusText(ProgressStatus status) {
		if (status.getProgress_status() == 9) {
			return "대출중지";
		} else if (status.getProgress_status() == 6) {
			return "대출완료";
		} else {
			if (status.getAccount_verify_yn() == 1
					&& status.getAttach_yn() == 1
					&& status.getCall_yn() ==1 ) {
				return "입금대기";
			} else {
				return "대출진행";
			}
		}
	}
	
	
	
}