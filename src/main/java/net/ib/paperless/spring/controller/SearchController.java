package net.ib.paperless.spring.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import net.ib.paperless.spring.common.UUIDs;
import net.ib.paperless.spring.domain.LoanInfo;
import net.ib.paperless.spring.domain.User;
import net.ib.paperless.spring.service.InquiryService;
import net.ib.paperless.spring.support.SessionManager;

@Controller
public class SearchController{
	
	private static final Logger logger = LoggerFactory.getLogger(SearchController.class);
	
	@Autowired
    InquiryService inquiryService;
	
	@Autowired
	SessionManager sessionManager;
	/**
	 * info page
	 * @param model
	 * @param loanId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/{loanId}/search/info"})
	public String searchInfo(Model model , @PathVariable String loanId) throws Exception {
		Map<String,Object> map = new HashMap<String,Object>();
		LoanInfo entity = inquiryService.loanInfoSelect(loanId);
		map.put("loanName", entity.getName());
		map.put("loanId", loanId);
		map.put("loanInfo", entity);
		model.addAttribute("map", map);
		//System.out.println("llll : " + loanId);
		//inquiryService.setLoanUserInfo("01095127232", loanId);
		return "/search/info";
	}
	
	/**
	 * step01 page
	 * @param model
	 * @param loanId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/{loanId}/search/step01"})
	public String firstStep(Model model , @PathVariable String loanId) throws Exception {
		Map<String,Object> map = new HashMap<String,Object>();
		LoanInfo entity = inquiryService.loanInfoSelect(loanId);

		Random generator = new Random();   
		String random_code = String.valueOf(1000+generator.nextInt(9000)); 
		
		map.put("userKey", random_code);
		map.put("loanName", entity.getName());
		map.put("loanId", loanId);
		map.put("loanInfo", entity);
		model.addAttribute("map", map);
		//System.out.println("llll : " + loanId);
		//inquiryService.setLoanUserInfo("01095127232", loanId);
		return "/search/step01";
	}
	
	/**
	 * step01 page
	 * @param model
	 * @param loanId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/{loanId}/search/step02",method={RequestMethod.GET , RequestMethod.POST})
	public String secondStep(Model model , @PathVariable String loanId,HttpServletRequest request,@RequestParam HashMap<String, String> paramsMap ) throws Exception {
		Map<String,Object> map = new HashMap<String,Object>();
		System.out.println("paramsMap : " + paramsMap);
		
		//----------Session 생성 ------------//
//		String telNumber = paramsMap.get("hp_num1")+paramsMap.get("hp_num2");
		String telNumber = paramsMap.get("hp_num2");
		
		logger.info("telNumber:{}", telNumber);
		String jumin = paramsMap.get("jumin_1")+paramsMap.get("jumin_2");

		LoanInfo loanInfo = inquiryService.loanInfoSelect(loanId);
		HttpSession session = request.getSession();
		User user = new User();
		user.setUserName(paramsMap.get("user_name"));
		user.setLoanId(loanId);
		user.setLoanName(loanInfo.getName());
		user.setUserPhone(telNumber);
		user.setRequireAmount(paramsMap.get("require_amount"));
		user.setJumin(jumin);
		user.setAuthKey(paramsMap.get("userKey"));
		String uuids = UUIDs.createNameUUID(telNumber.getBytes()).toString();
		user.setUserKey(uuids);
		session.setAttribute("tmp_user", user);
		/*
		String remoteAddr = request.getHeader("REMOTE_ADDR");
		session.setAttribute("remoteaddr", remoteAddr);
		sessionManager.setHttpSession(session.getId(), session, remoteAddr);
		*/
		//---------Session 생성 -------------//

		map.put("loanName", loanInfo.getName());
		map.put("loanId", loanId);
		model.addAttribute("map", map);
		//inquiryService.setLoanUserInfo("01095127232", loanId);
		return "/search/step02";
	}
	
	/**
	 * step03 page
	 * @param model
	 * @param loanId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping({"/{loanId}/search/step03"})
	public String thirdStep(Model model , @PathVariable String loanId,HttpServletRequest request) throws Exception {
		Map<String,Object> map = new HashMap<String,Object>();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("tmp_user");
		System.out.println("tmp_user : " + user);

		LoanInfo entity = inquiryService.loanInfoSelect(loanId);
		map.put("loanName", entity.getName());
		map.put("loanId", loanId);
		map.put("user", user);
		model.addAttribute("map", map);
		return "/search/step03";
	}
}