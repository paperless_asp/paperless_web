package net.ib.paperless.spring.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import net.ib.paperless.spring.common.ApiResponse;
import net.ib.paperless.spring.service.InquiryService;
import net.ib.paperless.spring.service.ProgressService;

@Controller
@RequestMapping("/appapi/**")
public class AppApiController{

	private static final Logger logger = LoggerFactory.getLogger(AppApiController.class);
	
	@Autowired
    InquiryService inquiryService;
	@Autowired
    ProgressService progressService;
	
	@RequestMapping(value="/appInfoCheckSelectOne",method={RequestMethod.GET , RequestMethod.POST},produces="application/json")
	@ResponseBody
	public ApiResponse<Map<String,Object>> appInfoCheckSelectOne(@RequestBody Map<String,Object> params){
		for (String key : params.keySet()) {
			logger.info("KEY-VALUE[{}]:[{}]", key, params.get(key));
		}
		int cnt =  inquiryService.appInfoCheckSelectOne(params);
		ApiResponse<Map<String , Object>> responseJson = new ApiResponse<Map<String,Object>>();
		responseJson.setResult(cnt>0?true:false);
		return responseJson;
	}
	
	@RequestMapping(value="/appInfoCheck",method={RequestMethod.GET , RequestMethod.POST})
	@ResponseBody
	public ApiResponse<Map<String,Object>> appInfoCheck(@RequestParam("os") String os,
			@RequestParam("version") String version,
			@RequestParam("package_name") String package_name,
			@RequestParam("hash") String hash){

		Map<String,String> params = new HashMap<String, String>();
		
		params.put("os", os);
		params.put("package_name", package_name);
		params.put("version", version);
		params.put("hash", hash);
		
		int cnt =  inquiryService.appInfoCheckSelectOne(params);
		ApiResponse<Map<String , Object>> responseJson = new ApiResponse<Map<String,Object>>();
		responseJson.setResult(cnt>0?true:false);
		return responseJson;
	}
}