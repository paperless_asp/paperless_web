package net.ib.paperless.spring.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import net.ib.paperless.spring.common.ApiResponse;
import net.ib.paperless.spring.common.UUIDs;
import net.ib.paperless.spring.domain.EformUserData;
import net.ib.paperless.spring.domain.IdentityAuthInfo;
import net.ib.paperless.spring.domain.LoanInfo;
import net.ib.paperless.spring.domain.User;
import net.ib.paperless.spring.service.InquiryService;
import net.ib.paperless.spring.service.ProgressService;
import net.ib.paperless.spring.support.SessionManager;

@Controller
@RequestMapping("/api/**")
public class PaperApiController{

	@Autowired
    InquiryService inquiryService;
	@Autowired
    ProgressService progressService;
	@Autowired
	SessionManager sessionManager;

	@RequestMapping(value="/setProgress",method={RequestMethod.GET , RequestMethod.POST},produces="application/json")
	@ResponseBody
	public ApiResponse<Map<String,Object>> progressRequest(@RequestBody Map<String,Object> params , HttpServletRequest request){
		System.out.println("params : " + params );
		
		ApiResponse<Map<String , Object>> responseJson = new ApiResponse<Map<String,Object>>();
		
		String phoneNumber = params.get("tel_number").toString();
		String name = params.get("user_name").toString();
		String loanId = params.get("loan_id").toString();
		String requireAmount = params.get("require_amount").toString();
		String authKey = params.get("auth_key").toString();
		String type = params.get("type").toString();
		
		IdentityAuthInfo identityAuthInfo1 = new IdentityAuthInfo();
		identityAuthInfo1.setName(name);
		identityAuthInfo1.setPhoneNumber(phoneNumber);
		identityAuthInfo1.setAuthKey(authKey);
		
		
		IdentityAuthInfo identityAuthInfo2 = progressService.indentityAuthInfoSelectOne(identityAuthInfo1);
		
		if(identityAuthInfo2 != null || type.equals("fast"))
		{
			LoanInfo entity = inquiryService.loanInfoSelect(loanId);
			
			if(Integer.parseInt(requireAmount) >= entity.getMin_amount() 
					&& Integer.parseInt(requireAmount) <= entity.getMax_amount())
			{

				progressService.identityAuthInfoDelete(identityAuthInfo1); //이전 인증내역이 있다면 삭제
				
				Map<String,Object> map = inquiryService.setLoanUserInfo(params);
				
				
				HttpSession session = request.getSession();
				User user = new User();
				user.setLoanId(loanId);
				user.setLoanName(entity.getName());
				user.setUserPhone(phoneNumber);
				String uuids = UUIDs.createNameUUID(phoneNumber.getBytes()).toString();
				user.setUserKey(uuids);
				session.setAttribute("user", user);

//				String sessionId = request.changeSessionId();
				String sessionId = request.getRequestedSessionId();
				System.out.println("SESSION-ID : " + sessionId);
				
				String remoteAddr = request.getHeader("REMOTE_ADDR");
				session.setAttribute("remoteaddr", remoteAddr);
				sessionManager.setHttpSession(sessionId, session, remoteAddr);

				
				responseJson.setResult((boolean) map.get("result"));
				responseJson.setMessage(map.get("msg").toString());
				return responseJson;
			}
			
			responseJson.setResult(false);
			responseJson.setMessage("인증정보가 올바르지 않습니다.");
			return responseJson;
		}
		
		responseJson.setResult(false);
		responseJson.setMessage("인증정보가 올바르지 않습니다.");
		return responseJson;
		
	}

	@RequestMapping(value="/makeUserSession",method={RequestMethod.GET , RequestMethod.POST},produces="application/json")
	@ResponseBody
	public ApiResponse<Map<String,Object>> makeUserSession(@RequestBody Map<String,Object> params , HttpServletRequest request){
		System.out.println("params : " + params );
		//Map<String,Object> map = inquiryService.setLoanUserInfo(params);
		String telNumber = params.get("tel_number").toString();
		LoanInfo loanInfo = inquiryService.loanInfoSelect(params.get("loan_id").toString());
		HttpSession session = request.getSession();
		User user = new User();
		user.setLoanId(params.get("loan_id").toString());
		user.setLoanName(loanInfo.getName());
		user.setUserPhone(telNumber);
		String uuids = UUIDs.createNameUUID(telNumber.getBytes()).toString();
		user.setUserKey(uuids);
		session.setAttribute("user", user);

//		String sessionId = request.changeSessionId();
		String sessionId = request.getRequestedSessionId();
		System.out.println("SESSION-ID : " + sessionId);
		
		String remoteAddr = request.getHeader("REMOTE_ADDR");
		session.setAttribute("remoteaddr", remoteAddr);
		sessionManager.setHttpSession(sessionId, session, remoteAddr);

		
		ApiResponse<Map<String , Object>> responseJson = new ApiResponse<Map<String,Object>>();
		responseJson.setResult(true);
		responseJson.setMessage("");
		return responseJson;
	}
	
	@RequestMapping(value="/setUserDataProgressStatus",method={RequestMethod.GET , RequestMethod.POST},produces="application/json")
	@ResponseBody
	@Transactional
	public ApiResponse<Map<String,Object>> setUserDataProgressStatus(@RequestBody Map<String,Object> params){
		ApiResponse<Map<String , Object>> responseJson = new ApiResponse<Map<String,Object>>();
		EformUserData entity = progressService.eformUserdataSelectOne("sanwa4");
		entity.setProgress_id(params.get("progress_id").toString());
		if(progressService.eformUserDataInsert(entity)>0){
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("call_yn", 1);
			/*map.put("eform_yn", 1);
			map.put("attach_yn", 1);
			map.put("account_transfer_yn", 1);
			map.put("account_verify_yn", 1);
			map.put("identity_chk_yn", 1);*/
			map.put("progressId", params.get("progress_id").toString());
			if(progressService.progressStatusUpdate(map)>0){
				responseJson.setResult(true);
				responseJson.setMessage("변경되었습니다.");
			}else{
				responseJson.setResult(false);
				responseJson.setMessage("실패했습니다.");
			}
		}else{
			responseJson.setResult(false);
			responseJson.setMessage("이미 유저데이터가 있습니다.");
		}
		
		return responseJson;
	}
	


	@RequestMapping(value="/checkLogin",method={RequestMethod.GET , RequestMethod.POST},produces="application/json")
	@ResponseBody
	public ApiResponse<Map<String,Object>> checkLogin(@RequestBody Map<String,Object> params){
			
		ApiResponse<Map<String , Object>> responseJson = new ApiResponse<Map<String,Object>>();
		responseJson.setResult(false);
		responseJson.setMessage("로그인이 필요합니다");
		responseJson.setUrl("/main");
		return responseJson;
	}
	
	@RequestMapping(value="/setIdentityAuthInfo",method={RequestMethod.GET , RequestMethod.POST},produces="application/json")
	@ResponseBody
	public ApiResponse<Map<String,Object>> setIdentityAuthInfo(@RequestBody Map<String,Object> params , HttpServletRequest request){
		
		ApiResponse<Map<String , Object>> responseJson = new ApiResponse<Map<String,Object>>();
		
		String phoneNumber = params.get("phoneNumber").toString();
		String name = params.get("name").toString();
		String userKey = params.get("userKey").toString();
		
		IdentityAuthInfo identityAuthInfo = new IdentityAuthInfo();
		identityAuthInfo.setName(name);
		identityAuthInfo.setPhoneNumber(phoneNumber);
		identityAuthInfo.setAuthKey(userKey);

		progressService.identityAuthInfoDelete(identityAuthInfo); //이전 인증내역이 있다면 삭제
		
		//인증정보가 저장되었으면 성공
		if(progressService.identityAuthInfoInsert(identityAuthInfo)>0){
			responseJson.setResult(true);
			responseJson.setMessage("인증정보 저장완료.");
			return responseJson;
		}
		
		responseJson.setResult(false);
		responseJson.setMessage("인증정보 저장실패.");
		return responseJson;
	}
	
}